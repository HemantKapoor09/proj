import { Component, OnInit, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { ApiService } from 'src/app/services/api.service';
import { Network } from '@ionic-native/network/ngx';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Socket } from 'ngx-socket-io';
import * as moment from 'moment';
import { AboutTeamComponent } from 'src/app/components/about-team/about-team.component';
import { DeviceNativeApiService } from 'src/app/services/device-native-api.service';
import { EventsCustomService } from 'src/app/services/events-custom.service';
import { AboutGroupChatComponent } from '../../../components/about-group-chat/about-group-chat.component';

type colorAssignmentType = {
  id: any,
  colorAssigned: string
};

@Component({
  selector: 'app-group-chat',
  templateUrl: './group-chat.page.html',
  styleUrls: ['./group-chat.page.scss'],
})

export class GroupChatPage implements OnInit {

  @ViewChild('messageToSendBox') messageBox;
  @ViewChild('content') private content: any;

  chat: any;
  chatInput: any = '';
  imageurls = [];
  fileArray = [];
  imagearray = [];
  fileLength: number;
  fileToUpload: any;
  fileExtension: any;
  fileToUploadFileName = [];
  userDetail: any;
  onlineMode: boolean;
  imageUrl = [];
  environment: any;
  previousUrl: string;
  limit = 500;
  offset = 0;
  groupDetails: any;
  groups: any;
  groupArrays: { date1: string; message: any; }[];
  today: any;
  yesterday: any;
  moreChat: any;
  firstTime = 0;
  groupId: any;
  groupMembers: any;
  ok: any;
  teamPic: any = '';
  groupMembArray = [];
  fileToUploadlist = [];
  adminDetail: any;
  groupCreated: any;
  noChat = false;
  colorToName = [];
  tmpArray = [];
  local: any;
  loadmoreColor = [];
  masteradminid: any;
  noGroupPic: boolean;
  showButton = false;
  showLoader: boolean;
  onGroupChatPage = '1';
  data1;

  // String
  todayString = 'Today';
  yesterdayString = 'Yesterday';
  failed: 'Failed';
  fileNotSupported: 'File not supported';
  groupChatLeft: any = '%X has left';
  groupChatRemoved: any = '%X removed';
  groupChatJoin: any = '%X joined';
  groupChatCreated: any = '%X created group %Y';
  groupMembString = '';
  teamName = '';
  youString = 'You';
  takePhoto = 'Take Photo';
  chooseImage = 'Choose Image';
  saySomethingString = 'Say something...';

  colrArr = ['#dc7708', 'cadetblue', 'black', 'blueviolet', 'brown', 'burlywood', 'chocolate', 'cornflowerblue', 'crimson', 'darkcyan', 'darkgoldenrod', 'darkgreen', 'darkmagenta', 'darkorchid', 'darkred', 'darkslategray', 'darkviolet', 'deeppink', 'dodgerblue', 'dimgrey', 'indigo', 'darkorange', 'lightcoral', 'lightslategray', 'hotpink', 'maroon', 'mediumorchid', 'midnightblue', 'navy', 'olive', 'olivedrab', 'orangered', 'palevioletred', 'peru', 'purple', 'rebeccapurple', 'rosybrown', 'red', 'salmon', 'sienna', 'slateblue', 'slategray', 'steelblue', 'tomato', '#07a', '#0BE', '#167', '#703', '#755', '#826'];


  constructor(
    private utilServ: GenralUtilsService,
    private apiService: ApiService,
    private network: Network,
    private modalController: ModalController,
    private actRouter: ActivatedRoute,
    private nativeLib: DeviceNativeApiService,
    private socket: Socket,
    private eventCustom: EventsCustomService) {
    this.today = moment(new Date()).format('YYYY-MM-DD');
    const tempDate = moment().subtract(1, 'days');
    this.yesterday = moment(tempDate).format('YYYY-MM-DD');

    this.actRouter.params.subscribe((res: any) => {
      this.userDetail = JSON.parse(this.utilServ.getUserDetails());
      this.groupDetails = JSON.parse(localStorage.getItem('teamData'));
      console.log(this.groupDetails);
      this.getGroupChat();
      this.apiService.groupMessageReadUpdate(this.userDetail.id, this.groupId, this.groupDetails.type).
        pipe().subscribe((data: any) => { });
      this.socket.on('receive-group-message', (data) => {
        this.getGroupChat();
        if (this.onGroupChatPage === '1') {
          this.apiService.groupMessageReadUpdate(this.userDetail.id, this.groupId, this.groupDetails.type).
            pipe().subscribe((response: any) => { });
        }
      });
    });

    if (network.type !== 'none') {
      this.onlineMode = true;
    } else {
      this.onlineMode = false;
    }
  }

  ionViewWillEnter() {
    // this.getGroupChat();
  }

  ngOnInit() {
    this.environment = environment;
  }

  async presentActionSheet() {

    this.nativeLib.presentActionSheetCamOrGall();
    this.eventCustom.subscribe('imageReady', (imgData) => {
      this.imagearray.push(imgData.webPath);
      this.fileToUploadlist.push(imgData.blob);
      // tslint:disable-next-line: prefer-for-of
      for (let s = 0; s < this.fileToUploadlist.length; s++) {
        this.apiService.chatimgupload(this.fileToUploadlist[s], this.userDetail.id, imgData.reqPath).pipe().subscribe(data => {
          this.data1 = data;
          this.imageurls.push(this.data1);
        },
          (error: any) => {
            console.log(' Error in select media section ::', error);
          });
      }
    });
  }

  sendMessageFun(message) {
    const msg = this.formatString(message);
    this.apiService.connectFuntion();
    // For text messages
    if (this.imageUrl.length === 0) {
      const data = {
        message: {
          group_id: this.groupId,
          group_type: this.groupDetails.type,
          group_name: this.teamName,
          msg_type: 0,
          content: msg,
          media: '',
          userinfo: { from_id: this.userDetail.id, name: this.userDetail.first_name }
        }
      };
      this.chatInput = '';
      this.socket.emit('send-group-message', data);
      // For Images
    } else if (this.imageUrl.length !== 0) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < this.imageUrl.length; i++) {
        const data = {
          message: {
            group_id: this.groupId,
            group_type: this.groupDetails.type,
            group_name: this.teamName,
            msg_type: 1,
            content: msg,
            media: this.imageUrl[i],
            userinfo: { from_id: this.userDetail.id, name: this.userDetail.first_name }
          }
        };
        this.chatInput = '';
        this.socket.emit('send-group-image-to-server', data);
      }
      this.imageurls.length = 0;
      this.imageUrl.length = 0;
      this.imagearray.length = 0;
      this.fileToUploadFileName.length = 0;
      this.fileLength = 0;
      this.fileArray.length = 0;
    }

  }

  logScrolling(event) {
    if (this.chat.length > 15) {
      if (event.detail.scrollTop > 600) {
        this.showButton = false;
      }
      else {
        this.showButton = true;
      }
    }
    else {
      this.showButton = false;
    }
  }

  ionViewWillLeave() {
    this.tmpArray = [];
    this.chat = [];
    this.colorToName = [];
    this.groupMembers = '';
    this.groupMembArray = [];
  }

  scrollToBottomOnInit() {
    this.content.scrollToBottom(50);
  }

  // getMissing(a, b) {
  //   let missings = [];
  //   let matches = false;
  //   for (let i = 0; i < a.length; i++) {
  //     matches = false;
  //     for (let e = 0; e < b.length; e++) {
  //       if (a[i] === b[e].id) matches = true;
  //     }
  //     if (!matches) missings.push(a[i]);
  //   }
  //   return missings;
  // }


  // uniqBy(a, key) {
  //   let seen = {};
  //   return a.filter(function (item) {
  //     let k = key(item);
  //     return seen.hasOwnProperty(k) ? false : (seen[k] = true);
  //   });
  // }

  loadMoreMessage(event) {
  }

  handleImgError(ev: any, item: any) {
    const source = ev.srcElement;
    const imgSrc = 'https://storage.googleapis.com/tribation_uploads_dev/noimage.png';
    source.src = imgSrc;
  }

  // isInarray(array, word) {
  //   return array.indexOf(word.toLowerCase()) > -1;
  // }

  handlefileinput(file: FileList) { }

  cancelUpload(i) {
    this.imageurls.splice(i, 1);
    this.imagearray = this.imageurls;
    this.fileArray.splice(i, 1);
    this.fileLength = this.fileArray.length;
    this.imageUrl.splice(i, 1);
  }

  removeimg(i) {
    if (i > -1) {
      this.apiService.deleteUploadImage(this.fileToUploadFileName[i], this.userDetail.id).pipe().subscribe((res: any) => {
        if (res.success === 1) {
          this.imageUrl.splice(i, 1);
          this.fileToUploadFileName.splice(i, 1);
          this.imageurls.splice(i, 1);
          this.fileArray.splice(i, 1);
          this.imagearray = this.imageurls;
          this.fileLength = this.fileArray.length;
        }
      });
    }
  }

  onClick(event) {
    event.target.value = '';
  }

  async groupDetail(id) {
    if (this.groupDetails.type === 2) {
      const modal = await this.modalController.create({
        component: AboutGroupChatComponent
      });
      modal.onDidDismiss().then((ref) => {
        if (ref.data.data === 0) {
          this.groupDetails = JSON.parse(localStorage.getItem('teamData'));
        }
      });
      return await modal.present();
    } else if (this.groupDetails.type === 1) {
      const modal = await this.modalController.create({
        component: AboutTeamComponent,
      });
      modal.onDidDismiss().then((ref) => {
        if (ref.data.data === 0) {
          this.groupDetails = JSON.parse(localStorage.getItem('teamData'));
        }
      });
      return await modal.present();
    }
  }


  back() {
    this.onGroupChatPage = '0';
    localStorage.removeItem('teamData');
    this.utilServ.navMainChat();
  }

  formatString(strng) {
    return this.utilServ.formatString(strng);
  }

  getGroupChat() {
    if (this.groupDetails.type === 1) {
      this.groupId = this.groupDetails.gid;
      this.apiService.getTeamDataById(this.groupId).pipe().subscribe((res: any) => {
        this.teamPic = res.message[0].team_pic;
        this.teamName = res.message[0].team_name;
        this.masteradminid = res.message[0].masteradminid;
        if (this.masteradminid !== this.userDetail.id) {
          this.apiService.getUserBaseInfoByUserId(this.masteradminid, this.masteradminid).pipe().subscribe((res: any) => {
            this.adminDetail = res.message.baseInfo;
            this.groupCreated = this.groupChatCreated.replace(/%X/g, this.adminDetail.first_name).replace(/%Y/g, this.teamName);
          });
        } else {
          this.groupCreated = this.groupChatCreated.replace(/%X/g, this.youString).replace(/%Y/g, this.teamName);
        }
      });
    }
    if (this.groupDetails.type === 2) {
      this.groupId = this.groupDetails.gid;
      this.apiService.getgroupchatdatabyId(this.groupId).pipe().subscribe((res: any) => {
        this.teamPic = res.message[0].profile_bg_img;
        this.teamName = res.message[0].group_name;
        if (this.teamPic === 'null' || this.teamPic === '') {
          console.log(this.teamPic)
          this.noGroupPic = true;
        }
        else {
          this.noGroupPic = false;
        }
        this.masteradminid = res.message[0].group_master;
        if (this.masteradminid !== this.userDetail.id) {
          this.apiService.getUserBaseInfoByUserId(this.masteradminid, this.masteradminid).pipe().subscribe((res: any) => {
            this.adminDetail = res.message.baseInfo;
            this.groupCreated = this.groupChatCreated.replace(/%X/g, this.adminDetail.first_name).replace(/%Y/g, this.teamName);
          });
        } else {
          this.groupCreated = this.groupChatCreated.replace(/%X/g, this.youString).replace(/%Y/g, this.teamName);
        }
      });
    }
    this.getChatHistory();
  }

  getChatHistory() {
    this.offset = 0;
    this.limit = 100;
    console.log(this.userDetail.id, this.groupId, this.offset, this.limit, this.groupDetails.type);
    this.apiService.getGroupChatHistroy(this.userDetail.id, this.groupId, this.offset, this.limit, this.groupDetails.type).
      pipe().subscribe((res: any) => {
        this.chat = res.message;
        console.log("fg", this.chat);
        // Colored Names Function
        let ttl = this.chat.length;
        this.tmpArray = [];
        for (let q = 0; q < ttl; q++) {
          const elem = this.chat[q];
          if ((elem.msg_type === 0 || elem.msg_type === 1) && elem.fromid !== this.userDetail.id) {
            this.tmpArray.push(elem.fromid);
          }
        }
        const xIndex = this.tmpArray.filter((n, i) => this.tmpArray.indexOf(n) === i);
        ttl = xIndex.length;
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < xIndex.length; i++) {
          const asd = {} as colorAssignmentType;
          asd.id = xIndex[i];
          asd.colorAssigned = this.colrArr[Math.floor(Math.random() * this.colrArr.length)];
          this.colorToName.push(asd);
        }
        // Colored Names Function Ends
        if (res.message.length === 0) {
          this.noChat = true;
        }

        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < this.chat.length; i++) {
          this.chat[i].time = this.chat[i].time || '';
          this.chat[i].color = this.chat[i].color || '';
          this.local = moment.utc(this.chat[i].created).local();
          this.chat[i].time = moment(this.local).format('hh:mm a');
          if (this.chat[i].msg_type === 2) {
            this.chat[i].content = this.groupChatLeft.replace(/%X/g, this.chat[i].first_name);
          }
          if (this.chat[i].msg_type === 3) {
            if (this.chat[i].fromid === this.userDetail.id) {
              this.chat[i].content = this.groupChatJoin.replace(/%X/g, this.youString);
            } else {
              this.chat[i].content = this.groupChatJoin.replace(/%X/g, this.chat[i].first_name);
            }
          }
          if (this.chat[i].msg_type === 5) {
            this.chat[i].content = this.groupChatRemoved.replace(/%X/g, this.chat[i].first_name);
          }

          // tslint:disable-next-line: prefer-for-of
          // for (let c = 0; c < this.colorToName.length; c++) {
          //   if (this.chat[i].fromid === this.colorToName[c].id) {
          //     this.chat[i].namecolor = this.colorToName[c].colorAssigned;
          //   }
          // }
        }
        this.offset += this.chat.length;
        const groups = this.chat.reduce((groups, message) => {
          const date = moment(message.created).format('YYYY-MM-DD');
          if (!groups[date]) {
            groups[date] = [];
          }
          groups[date].push(message);
          return groups;
        }, {});
        this.groups = groups;
        this.groupArrays = Object.keys(groups).map((date) => {
          let date1: any;
          if (this.today === date) {
            date1 = this.todayString;
          } else if (this.yesterday === date) {
            date1 = this.yesterdayString;
          } else {
            date1 = date;
          }
          return {
            date1,
            message: groups[date]
          };
        });
        setTimeout(() => {
          this.scrollToBottomOnInit();
        }, 2000);
      }), err => { };

    if (this.groupDetails.type == 1) {
      this.apiService.getTeamMembers(this.groupId).pipe().subscribe((res: any) => {
        this.groupMembers = res.message;
        // console.log(this.groupMembers);
        this.groupMembArray = [];
        for (let z = 0; z < this.groupMembers.length; z++) {
          if (this.groupMembers[z].member_accepted == 1) {
            console.log('this.groupMembers[z]');
            console.log(this.groupMembers[z]);
            this.groupMembArray.push(this.groupMembers[z]);
          }
        }
        let index = this.groupMembArray.findIndex(x => x.userid === this.userDetail.id);
        this.groupMembArray[index].first_name = this.youString;
        this.groupMembString = Array.prototype.map.call(this.groupMembArray, function (item) {
          return item.first_name;
        }).join(',');
      });
    }
    if (this.groupDetails.type === 2) {
      this.apiService.getGroupchatMembers(this.groupId).pipe().subscribe((res: any) => {
        this.groupMembers = res.message;
        console.log('fdgh', this.groupMembers);
        this.groupMembArray = [];
        this.groupMembers.forEach(element => {
          if (element.userid !== this.userDetail.id && element.member_accepted === '1') {
            this.groupMembArray.push(element.first_name);
          } else if (element.userid === this.userDetail.id && element.member_accepted === '1') {
            this.groupMembArray.push(this.youString);
          }
        });
        this.groupMembString = this.groupMembArray.toString();
      });
    }
    setTimeout(() => {
      this.messageBox.setFocus()
    }, 500);
  }

  openKeyboard() {
    this.utilServ.showKeyBoard();
  }

}
