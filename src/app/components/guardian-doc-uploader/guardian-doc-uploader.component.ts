import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';

@Component({
  selector: 'app-guardian-doc-uploader',
  templateUrl: './guardian-doc-uploader.component.html',
  styleUrls: ['./guardian-doc-uploader.component.scss'],
})
export class GuardianDocUploaderComponent implements OnInit {
  @ViewChild('userInput') userInputViewChild: ElementRef;
  fName;
  userInputElement: HTMLInputElement;
  lName;
  userDetail;
  guardianString = 'Guardian';
  firstNameString = 'First Name';
  lastNameString = 'Last Name';
  submitString = 'Submit';

  constructor(
    private actRouter: ActivatedRoute,
    private modalController: ModalController,
    private apiService: ApiService,
    private utilServ: GenralUtilsService) {
    this.userDetail = JSON.parse(this.utilServ.getUserDetails());
  }

  ngOnInit() {
    if (this.utilServ.langSetupFLag) {
      this.firstNameString = this.utilServ.getLangByCode('first_name');
      this.lastNameString = this.utilServ.getLangByCode('last_name');
    }
  }

  ngAfterViewInit() {
    this.userInputElement = this.userInputViewChild.nativeElement;
  }

  back() {
    this.modalController.dismiss();
  }
  handlefileinputIdCard(x) {
    this.utilServ.okButtonMessageAlert('Pick your Doc or image to be Guardian :placeholder:');
    // this.userInputElement.click();
    console.log(x);
  }

}
