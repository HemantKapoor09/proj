import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-comment-on-post',
  templateUrl: './comment-on-post.component.html',
  styleUrls: ['./comment-on-post.component.scss'],
})
export class CommentOnPostComponent implements OnInit {
  @ViewChild('inputComment') inputComment: any;
  postData: any;
  userDetail: any;
  environment: any;
  ifComment = false;
  commentData: any[] = [];
  commentText: string;
  replyData: any;
  temp;

  // String
  noCommentsString = 'No comments';
  replyString = 'Reply';
  replysString = 'Reply\'s';

  constructor(
    private actRouter: ActivatedRoute,
    private modalController: ModalController,
    private apiService: ApiService,
    private utilServ: GenralUtilsService) {
    this.actRouter.queryParams.subscribe(() => {
      this.postData = JSON.parse(localStorage.getItem('commentOnPost'));
      // console.log('COnst');
    });
  }

  ngOnInit() {
    this.environment = environment;
    // console.log('ini');

    this.userDetail = JSON.parse(this.utilServ.getUserDetails());
    if (this.utilServ.langSetupFLag) {
      this.replyString = this.utilServ.getLangByCode('post_reply');
      this.replysString = this.utilServ.getLangByCode('post_replys');
      // this.noCommentsString = this.utilServ.getLangByCode('post_replys');
    }
    this.getCommentData();
  }
  getCommentData() {
    console.log('data');
    this.apiService.getsharelikecommentpost(this.userDetail.id, this.postData.postId).subscribe((commentRes: any) => {
      console.log(commentRes.message);
      const x = commentRes.message.commentData.comments;
      x.forEach(e => {
        e.showReply = false;
        this.commentData.push(e);
        // console.log(e);
      });
      if (this.commentData.length > 0) {
        this.ifComment = true;
      }
      console.log(this.commentData);
    });
  }
  loadReply(x) {
    this.commentData[x].showReply = !(this.commentData[x].showReply);
  }
  trimText() {
    this.commentText = this.utilServ.formatString(this.commentText);
  }
  replyToComment(commentId, commenterUser, comment, x) {
    // console.log('replty to a comment', x);
    // console.log(
    //   '\ntoken', this.userDetail.devicetoken,
    //   '\npost_id', this.postData,
    //   '\nreply_content', this.commentText,
    //   '\nuser_id', this.userDetail.id,
    //   this.replyData);
    this.replyData = {
      commeNtId: commentId,
      commenTer: commenterUser,
      commeNt: comment,
      serial: x
    };
  }
  postComment() {
    this.trimText();
    if (this.commentText !== '') {
      if (this.replyData) {
        this.apiService.addReply(
          this.userDetail.devicetoken,
          this.postData.postId,
          this.commentText,
          this.userDetail.id,
          this.replyData.commeNtId,
          this.replyData.commenTer.id).subscribe((res: any) => {
            if (res.success === 1) {
              const timeStamp = this.utilServ.getTimeStamp();
              const reply = {
                r_first_name: this.userDetail.first_name,
                r_id: this.userDetail.id,
                r_last_name: this.userDetail.last_name,
                r_profile_img_url: this.userDetail.profile_img_url_thump,
                reply_content: this.commentText,
                reply_created: timeStamp
              };
              const i = this.replyData.serial;
              this.commentData[i].replys.push(reply);
              console.log(this.commentData);
              this.replyData = null;
              this.commentText = null;
            }
          });
      } else {
        this.apiService.addComment(
          this.userDetail.devicetoken,
          this.postData.postId,
          this.commentText,
          this.userDetail.id,
          this.postData.userId
        ).subscribe((res: any) => {
          console.log(this.commentData);
          console.log(res);
          if (this.commentData.length === 0) {
            this.getCommentData();
            this.commentText = null;
          } else {
            if (res.success === 1) {

              const comment = res.message[0];
              const temp = {
                comment: comment.comment,
                created: comment.created,
                id: comment.id,
                replys: [],
                showReply: false,
                user: {
                  first_name: comment.first_name,
                  last_name: comment.last_name,
                  id: comment.user_id,
                  profile_img_url: comment.profile_img_url
                }
              };
              setTimeout(() => {
                this.commentData.push(temp);
                this.commentText = null;
              }, 150);
            }
          }
        });
      }
      console.log(
        '\ntoken', this.userDetail.devicetoken,
        '\npost_id', this.postData.postId,
        '\nreply_content', this.commentText,
        '\nuser_id', this.userDetail.id,
        '\npost_user_id', this.postData.userId,
        '\n',
        this.replyData);
    }
  }
  setFocus(nextElement) {
    nextElement.setFocus();
  }

  close() {
    console.log(  this.commentData);
    this.modalController.dismiss();
  }
}
