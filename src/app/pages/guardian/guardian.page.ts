import { Component, OnInit } from '@angular/core';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { ModalController } from '@ionic/angular';
import { GuardianDocUploaderComponent } from 'src/app/components/guardian-doc-uploader/guardian-doc-uploader.component';

@Component({
  selector: 'app-guardian',
  templateUrl: './guardian.page.html',
  styleUrls: ['./guardian.page.scss'],
})
export class GuardianPage implements OnInit {

  constructor(
    private actRouter: ActivatedRoute,
    private modalController: ModalController,
    private apiService: ApiService,
    private utilServ: GenralUtilsService) {

    this.docUploder();
  }

  ngOnInit() {


  }
  takePictures() {
    console.log('hemant');
  }

  async docUploder() {
    const modal = await this.modalController.create({
      component: GuardianDocUploaderComponent,
      componentProps: {
        // custom_data: data,
        // team_data: d,
      }
    });
    modal.onDidDismiss().then(() => {
      // this.getEvents();
    });
    return await modal.present();
  }
}
