import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, LoadingController, AlertController,  IonSelect } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Socket } from 'ngx-socket-io';
import { ApiService } from 'src/app/services/api.service';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { FcmServiceService } from 'src/app/services/fcm-service.service';
import { async } from 'rxjs/internal/scheduler/async';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  public toplogo = 'logo.png';
  loaderToShow: Promise<void>;
  log: FormGroup;
  devicetoken: any;
  loginUserDetail: any;
  isLoading: boolean;
  signupp: any;
  languagee: any;
  getlanguage: any;
  username: any;
  device: any;
  pswrd: any = '';
  mobileOffline: string;
  showPasswordText: any;

  // Init
  hide = true;
  passwordType = 'password';
  passwordIcon = 'eye-off';


  // Strings
  emaiL = 'Email';
  passworD = 'Password';
  signInString = 'Sign In';
  dontHaveAccString = 'Don\'t have an account?';
  signUpString = 'Sign up';
  forgotPassString = 'Forgot Password';
  pleaseWaitString = 'Please Wait…';
  addSectionString = 'Add Section';

  constructor(
    private loadingController: LoadingController,
    private socket: Socket,
    private api: ApiService,
    private alertController: AlertController,
    private actRouter: ActivatedRoute,
    private router: Router,
    private utilServ: GenralUtilsService,
    private fcmServ: FcmServiceService) {
    this.actRouter.queryParams.subscribe(() => {
      console.log('Login Sending data', JSON.parse(this.utilServ.getUserDetails()));
      const logInOrNot = this.utilServ.checkAlredyLoggedIn();
      if (logInOrNot) {
        this.utilServ.navTimeline();
        this.api.reloadApp();
      } else {
        this.utilServ.navLogin();
      }
    });
  }

  ngOnInit() {
    this.createform();
  }

  private createform() {
    this.log = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@'
        + '[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]),
    });
  }

  emailCheck() {
    this.username = this.log.value.username.trim();
    this.username = this.username.replace(/\s/g, '');
  }

  rootpage() {
    this.hideLoader();
    // this.hide = !this.hide;
    this.devicetoken = localStorage.getItem('devicetoken');
    this.device = localStorage.getItem('device');
    if (!this.devicetoken) {
      this.devicetoken = this.fcmServ.newToken();
    }
    this.api.userlogin(this.log.value.username, this.log.value.password, this.devicetoken, this.device)
      .pipe()
      .subscribe((res: any) => {
        if (res.success === 0) {
          this.utilServ.okButtonMessageAlert(res.message);
          // this.hide = true;
        } else if (res.success === 1) {
          localStorage.setItem('userdetail', JSON.stringify(res.message));
          this.utilServ.setUpUser();
          this.showLoader();
          this.log.reset();
          // localStorage.setItem('username', this.log.value.username);
          // localStorage.setItem('language', res.message.language);

          // this.events.publish('language', res.message.language);
          // this.events.publish('scoutstatus', res.message.scout_status);
          // this.events.publish('userId', res.message.id);
          // this.events.publish('birthday', res.message.birthday);
          // this.events.publish('runappcompoent');

          // this.socket.emit('login', { id: res.message.tokenid });
          // this.api.getguardianstatusCount(res.message.id).subscribe((res1: any) => {
          //   this.events.publish('guardianCount', res1.message[0].count);
          //  // this.hide = !this.hide;
          // });
        }
        // console.log(this.utilServ.getUserDetails());

      },
        error => {
          console.log('Login Error', error);
          this.utilServ.noNetworkAlert();
        });
  }


  signup() {
    this.utilServ.navSignup();
  }

  resetPass() {
    this.utilServ.navForgetPass();
  }

  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: this.pleaseWaitString
    }).then((res) => {
      res.present();
      res.onDidDismiss().then((dis) => {
        alert('dissmiss');
        this.utilServ.navTimeline();
      });
    });
    this.hideLoader();
  }

  hideLoader() {
    this.api.reloadApp();
    this.utilServ.setUpUser();
    setTimeout(() => {
      this.loadingController.dismiss();
    }, 3000);
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  moveFocus(nextElement) {
    nextElement.setFocus();
  }
}
