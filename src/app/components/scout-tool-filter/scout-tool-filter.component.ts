import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SelectCountryComponent } from '../select-country/select-country.component';
import { SelectSportsOrLagacyComponent } from '../select-sports-or-lagacy/select-sports-or-lagacy.component';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-scout-tool-filter',
  templateUrl: './scout-tool-filter.component.html',
  styleUrls: ['./scout-tool-filter.component.scss'],
})
export class ScoutToolFilterComponent implements OnInit {

  country;
  countryCode = null;
  city;
  gender;
  position;
  sports;
  sportsId: number = null;
  sportsIcon: any;
  heightRange: any = {
    lower: 1,
    upper: 100
  };
  minHeight = 1;
  maxHeight = 100;
  diffHeight = 2;
  heightUnit;
  weightRange: any = {
    lower: 1,
    upper: 100
  };
  minWeight = 1;
  maxWeight = 100;
  diffWeight = 2;
  weightUnit;
  expRange: any = {
    lower: 1,
    upper: 80
  };
  minExp = 0;
  maxExp = 80;
  diffExp = 1;
  ageRange: any = {
    lower: 1,
    upper: 99
  };
  minAge = 1;
  maxAge = 99;
  diffAge = 1;
  expChange = false;
  heightChange = false;
  ageChange = false;
  weightChange = false;

  // Strings
  genderString = 'Gender';
  maleString = 'Male';
  femaleString = 'Female';
  countryString = 'Country';
  cityString = 'City';
  sportsSting = 'Sports';
  selectCounrtyString = 'Select Country';
  enterCityString = 'Enter City';
  positionString = 'Position';
  enterPositionString = 'Enter Position ';
  selectSportString = 'Select Sports';
  heightString = 'Height';
  inchesString = 'inches';
  cmString = 'cm';
  feetString = 'feet';
  weightString = 'Weight';
  kgString = 'Kg';
  poundsString = 'Pounds';
  stonesString = 'Stones';
  filterToolsString = 'Filter Tools';
  experinceString = 'Experience';
  ageString = 'Age Range';

  constructor(
    private modalController: ModalController,
    private actRouter: ActivatedRoute,
    private utilServ: GenralUtilsService) {
  }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.setupBasic();
  }
  ionViewDidLeave() {
    this.reset();
  }
  setupBasic() {
    this.heightUnit = 'in';
    this.weightUnit = 'pounds';
    // this.weightRange.lower = 0;
    // this.weightRange.upper = 100;
    // this.heightRange.lower = 0;
    // this.heightRange.upper = 100;
    this.ageRange.lower = 1;
    this.ageRange.upper = 99;
    this.minAge = 1;
    this.maxAge = 99;
    this.diffAge = 1;

    this.expRange.lower = 1;
    this.expRange.upper = 80;
    this.minExp = 0;
    this.maxExp = 80;
    this.diffExp = 1;
    this.setWeightRange();
    this.setHeightRange();
    this.reset();
  }
  weightRangeChanged(){
    this.weightChange = true;
  }
  changeWeight(x) {
    this.weightRange = {
      lower: Number(x.lower),
      upper: Number(x.upper)
    };
  }
  setWeightRange() {
    switch (this.weightUnit) {
      case 'kg':
        {
          this.minWeight = 20;
          this.maxWeight = 140;
          this.diffWeight = 5;
          break;
        }
      case 'pounds':
        {
          this.minWeight = 20;
          this.maxWeight = 300;
          this.diffWeight = 10;
          break;
        }
      case 'stones':
        {
          this.minWeight = 2;
          this.maxWeight = 30;
          this.diffWeight = 2;
          break;
        }
    }
    this.weightRange = {
      lower: Number(this.minWeight),
      upper: Number(this.maxWeight)
    };
  }

  heightRangeChanged(){
    this.heightChange = true;
  }
  changeHeight(x) {
    this.heightRange = {
      lower: Number(x.lower),
      upper: Number(x.upper)
    };
  }
  setHeightRange() {
    switch (this.heightUnit) {
      case 'in':
        {
          this.minHeight = 2;
          this.maxHeight = 200;
          this.diffHeight = 5;
          break;
        }
      case 'cm':
        {
          this.minHeight = 80;
          this.maxHeight = 250;
          this.diffHeight = 5;
          break;
        }
      case 'feet':
        {
          this.minHeight = 3;
          this.maxHeight = 10;
          this.diffHeight = 1;
          break;
        }
    }
    this.heightRange = {
      lower: Number(this.minHeight),
      upper: Number(this.maxHeight)
    };
  }

  changeExp(x) {
    console.log('changing Exp');
    this.expChange = true;
    this.expRange = {
      lower: Number(x.lower),
      upper: Number(x.upper)
    };
  }
  changeAge(x) {
    console.log('changing Age');
    this.ageChange = true;
    this.ageRange = {
      lower: Number(x.lower) || 0,
      upper: Number(x.upper) || 0
    };
  }

  async selectCountry() {
    const modal = await this.modalController.create({
      component: SelectCountryComponent,
    });

    modal.onDidDismiss().then(data => {
      const xDx = data.data.data;
      this.country = xDx.countryName;
      this.countryCode = xDx.countryCode;
    });
    return await modal.present();
  }
  async selectSports() {
    const modal = await this.modalController.create({
      component: SelectSportsOrLagacyComponent,
    });

    modal.onDidDismiss().then(data => {
      const xDx = data.data.data;
      this.sports = xDx.sportsName;
      this.sportsIcon = xDx.sportsIcon;
      this.sportsId = xDx.id;
      console.log(xDx);
    });
    return await modal.present();
  }

  reset() {
    this.gender = null;
    this.country = null;
    this.city = null;
    this.sports = null;
    this.sportsIcon = null;
    this.position = null;

    this.heightRange.lower = 1;
    this.heightRange.upper = 100;
    this.minHeight = 1;
    this.maxHeight = 100;
    this.diffHeight = 2;
    this.heightUnit = 'in';

    this.weightRange.lower = 1;
    this.weightRange.upper = 100;
    this.minWeight = 1;
    this.maxWeight = 100;
    this.diffWeight = 2;
    this.weightUnit = 'pounds';

    this.expRange.lower = 1;
    this.expRange.upper = 80;
    this.minExp = 0;
    this.maxExp = 80;
    this.diffExp = 1;

    this.ageRange.lower = 1;
    this.ageRange.upper = 99;
    this.minAge = 1;
    this.maxAge = 99;
    this.diffAge = 1;

    this.expChange = false;
    this.heightChange = false;
    this.ageChange = false;
    this.weightChange = false;
    this.sportsId = null;
  }
  save() {
    let x = {
      expRangeFrom: null,
      expRangeTo: null,
      ageRangeFrom: null,
      ageRangeTo: null,
      heightRangeFrom: null,
      heightRangeTo: null,
      weightRangeFrom: null,
      weightRangeTo: null,
      gender: this.gender,
      sports: this.sportsId,
      position: this.position,
      country: this.countryCode,
      city: this.city,
      weightUnit: null,
      heightUnit: null
    };

    if (this.expChange === true) {
      x.expRangeFrom = this.expRange.lower;
      x.expRangeTo = this.expRange.upper;
    }
    if (this.ageChange === true) {
      x.ageRangeFrom = this.ageRange.lower;
      x.ageRangeTo = this.ageRange.upper;
    }
    if (this.heightChange === true) {
      x.heightUnit = this.heightUnit;
      x.heightRangeFrom = this.heightRange.lower;
      x.heightRangeTo = this.heightRange.upper;
    }
    if (this.weightChange === true) {
      x.weightUnit = this.weightUnit;
      x.weightRangeFrom = this.weightRange.lower;
      x.weightRangeTo = this.weightRange.upper;
    }
    // console.log(' :: From Comp scout :: ');
    // console.table(x);

    // console.log('expChange', this.expChange);
    // console.log('heightChange', this.heightChange);
    // console.log('ageChange', this.ageChange);
    // console.log('weightChange', this.weightChange);
    this.modalController.dismiss({
      dismissed: true,
      data: x
    });
    this.reset();

  }
  moveFocus(nextElement) {
    nextElement.setFocus();
  }
  exitFocus() {
    this.utilServ.exitKeyBoard();
  }
  back() {
    this.modalController.dismiss({
      dismissed: true,
      data: null
    });
    this.reset();
  }
}
