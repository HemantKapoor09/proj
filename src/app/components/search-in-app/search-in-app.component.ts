import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ModalController } from '@ionic/angular';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';



@Component({
  selector: 'app-search-in-app',
  templateUrl: './search-in-app.component.html',
  styleUrls: ['./search-in-app.component.scss'],
})
export class SearchInAppComponent implements OnInit {
  environment;
  searchTerm = '';
  viewUserInfo: any;
  listOfUser: any;
  userListLength: any;


  // Strings

  searchString = 'search';
  noFriendsListString = 'No group or friend found';
  unfriendString = 'Unfriend';
  findFriendsGroupsString = 'Find your friends and groups';

  constructor(
    private modalController: ModalController,
    private apiService: ApiService,
    private router: Router,
    private utilServ: GenralUtilsService) {
    this.environment = environment;
  }

  ngOnInit() {
    if (this.utilServ.langSetupFLag) {
      this.searchString = this.utilServ.getLangByCode('search');
      this.noFriendsListString = this.utilServ.getLangByCode('no_frnd_list');
      this.unfriendString = this.utilServ.getLangByCode('unfriend');
      this.findFriendsGroupsString = this.utilServ.getLangByCode('search_keyword');
    }
  }
  ionViewDidEnter() {
    this.search(this.searchTerm);
  }

  search(x) {
    if (x) {
      this.apiService.serachInApp(x).pipe().subscribe((res: any) => {
        this.viewUserInfo = res.message.users;
        this.listOfUser = this.viewUserInfo;
        this.userListLength = this.listOfUser.length;
      });
    }
  }
  friendDetail(userId) {
    this.back();
    this.router.navigate([`profile/${userId}`]);
  }

  back() {
    this.modalController.dismiss();
  }
  unFriend(frndId, frndName) {

  }

}
