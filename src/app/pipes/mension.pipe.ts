import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'mension'
})
export class MensionPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) { }

  transform(html) {
    console.log(html);
    // return this.sanitizer.bypassSecurityTrustResourceUrl(html);
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }


}
