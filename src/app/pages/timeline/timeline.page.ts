import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiService } from 'src/app/services/api.service';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DeviceNativeApiService } from 'src/app/services/device-native-api.service';
import { MentionConfig } from 'angular-mentions';
import * as moment from 'moment';
import { exit, exitCode } from 'process';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { CommentOnPostComponent } from 'src/app/components/comment-on-post/comment-on-post.component';
import { EditPostComponent } from 'src/app/components/edit-post/edit-post.component';
import { DomSanitizer } from '@angular/platform-browser';
import { CameraSource} from '@capacitor/core';
import { EventsCustomService } from 'src/app/services/events-custom.service';

import { File, DirectoryEntry } from '@ionic-native/file/ngx';
import { MultimediaViewComponent } from 'src/app/components/multimedia-view/multimedia-view.component';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
window.URL = window.URL || window.webkitURL;

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.page.html',
  styleUrls: ['./timeline.page.scss'],
})
export class TimelinePage implements OnInit {
  // @ViewChild('avatarTemplate') avatarTemplate:any;
  @ViewChild('postText') postText: any;
  @ViewChild('popUpEdit') popUpEdit: any;
  @ViewChild('userInput') userInputViewChild: ElementRef;

  customAlertOptions: any;
  // Basic User Details
  userDetail;
  profileImageURL;
  profileFirstname;
  profileLastName;
  profilePicUrl;
  profileId;
  postsArray: any[] = [];
  strMn;
  legacySportIcon = '';
  environment;
  postContent: string;
  tempPostContent: string;
  showPostButton = false;
  friendsList;
  mentionConfig: MentionConfig;
  showMensionPickUp = false;
  // Strings
  timelineString = 'timeline';
  postString = 'Post';
  sharedString = 'Shared';
  someonesPostString = '\'s post';
  editString = 'Edit';
  deleteString = 'Delete';
  reportString = 'Report';
  tempFriendTag: any;
  tempMensionString = '';
  tempReplacement: any[] = [];
  innerTemp = '';
  firstTimeTag = true;
  tempString = '';
  atCount: any;
  currentSerachTag;
  currentSearchArray: string[];
  temprary = false;
  totalPost: any;
  limitReached = true;

  userInputElement: HTMLInputElement;
  mediaUrl: HTMLImageElement;
  processing: boolean;
  mediaArray = [];
  imagePath;
  files: FileList;
  imageExtArray = ['image/gif', 'image/jpeg', 'image/png', 'image/raw', 'image/svg', 'image/heic'];
  vidExtArray = ['video/webm', 'video/mpg', 'video/ogg', 'video/mp4', 'video/avi', 'video/mov'];
  xyz: any;
  searchFriendsList: any[] = [];
  progressBarval = '0';

  constructor(
    private sanitizer: DomSanitizer,
    private apiService: ApiService,
    private element: ElementRef,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController,
    private modalcontrolller: ModalController,
    private router: Router,
    private utilServ: GenralUtilsService,
    private actRouter: ActivatedRoute,
    private mediaCapture: MediaCapture,
    public nativeLib: DeviceNativeApiService,
    private eventCustom: EventsCustomService,
    private file: File) {
    this.actRouter.queryParams.subscribe(() => {
      this.setupBasicDetails();
      this.dontReloadMension();
    });
  }

  ngOnInit() {
    this.environment = environment;
    this.setupBasicDetails();
    this.customAlertOptions = {
      animated: true,
      translucent: true,
      mode: 'ios',
      cssClass: 'addSectionProtfolio',
    };
    setTimeout(() => {
      this.friendsList = this.utilServ.friendsList;
      this.searchFriendsList = this.friendsList;
      this.loadPost();
    }, 2000);
  }

  ionViewWillEnter() {
    this.utilServ.checkUserExists();
  }

  ngAfterViewInit() {
    this.userInputElement = this.userInputViewChild.nativeElement;
  }

  setupBasicDetails() {
    this.userDetail = JSON.parse(this.utilServ.getUserDetails());
    this.profileId = this.userDetail.id;
    this.profileImageURL = this.userDetail.profile_img_url_thump;
    this.profileFirstname = this.userDetail.first_name;
    this.profileLastName = this.userDetail.last_name;
    if (!this.userDetail) {
      this.utilServ.navLogin();
      window.location.reload();
    }
    if (this.utilServ.langSetupFLag) {
      this.timelineString = this.utilServ.getLangByCode('timeline');
    }
    // this.searchString = this.utilServ.getLangByCode('search');
    // this.teamString = this.utilServ.getLangByCode('team');
    // this.myprofileString = this.utilServ.getLangByCode('myprofile');
    // this.eventsString = this.utilServ.getLangByCode('events');
    // this.helpcenterString = this.utilServ.getLangByCode('help');
    // this.logoutString = this.utilServ.getLangByCode('logout');
    // this.doYouWantLogoutString = this.utilServ.getLangByCode('do_you_want_logout');
    // this.areYouSureString = this.utilServ.getLangByCode('are_you_sure');
    // this.cancelString = this.utilServ.getLangByCode('cancel');
    // this.yesString = this.utilServ.getLangByCode('yes');
    // this.apiService.deletePostTimeline(244).subscribe((a: any) => { console.log(a) });
  }

  doRefresh(event) {
    setTimeout(() => {

      event.target.complete();
    }, 1200);

  }

  postContentEdit(x: string) {
    if (this.showMensionPickUp === false) {
      this.atCount = x.length;
    }
    this.currentSearchArray = x.split('@');
    this.currentSerachTag = this.currentSearchArray[this.currentSearchArray.length - 1];
    if (this.currentSearchArray.length > 1 && this.showMensionPickUp === true) {
      console.log('curentsearch', this.currentSerachTag.length, ' : ', this.currentSerachTag);
    }

    if (this.showMensionPickUp === true) {
      this.search(this.currentSerachTag);
    }

    const strng = this.utilServ.formatString(this.postContent);
    if ((this.postContent === '' || strng === '') && this.mediaArray.length === 0) {
      this.showPostButton = false;
    } else {
      this.showPostButton = true;
    }
  }

  onFriendTag(x) {
    this.tempFriendTag = { id: x.id, name: x.name };
    const t = this.occurrences(this.tempFriendTag.name, ' ', false);
    // tslint:disable-next-line: prefer-for-of
    for (let index = 0; index < t; index++) {
      this.tempFriendTag.name = this.tempFriendTag.name.replace(' ', '_');
      // const element = array[index];
    }
    this.tempFriendTag.name = this.tempFriendTag.name.toUpperCase();
    const ttmp = {
      replaced: this.tempFriendTag.name,
      name: x.name,
      id: this.tempFriendTag.id,
    };
    this.postContent = this.postContent.substr(0, this.atCount) + '' + this.tempFriendTag.name + ' ';
    this.tempReplacement.push(ttmp);
    this.showMensionPickUp = false;
    this.moveFocus(this.postText);
    this.searchFriendsList = this.friendsList;
  }
  search(x) {
    this.searchFriendsList = this.filter(this.friendsList, x || null);
  }
  filter(items: any[], terms: string): any[] {
    if (!items) { return []; }
    if (!terms) { return items; }
    terms = terms.toLowerCase();
    return items.filter(it => {
      return it.name.toLowerCase().includes(terms);
    });
  }

  occurrences(stng, subString, allowOverlapping) {
    stng += '';
    subString += '';
    if (subString.length <= 0) { return (stng.length + 1); }
    let n = 0;
    let pos = 0;
    const step = allowOverlapping ? 1 : subString.length;
    while (true) {
      pos = stng.indexOf(subString, pos);
      if (pos >= 0) {
        ++n;
        pos += step;
      } else { break; }
    }
    return n;
  }


  loadMorePost(eve) {
    setTimeout(() => {
      if (this.postsArray.length === this.totalPost) {
        this.limitReached = true;
      } else {
        this.loadPost();
      }
      eve.target.complete();
    }, 1000);
  }
  postLegacySelect() {
    console.log('sports');
  }

  async openPopUpEdit(x, post, userId) {
    console.log('x', x, '\npost', post, '\nuserId ', userId)
    if (this.userDetail.id === userId && post.shared === 0) {
      const actionSheet = await this.actionSheetController.create({
        // mode: 'ios',
        // header: 'Albums',
        // cssClass: 'my-custom-class',
        buttons: [{
          text: this.deleteString,
          handler: () => {
            this.deletePost(x, post.id);
          }
        }, {
          text: this.editString,
          handler: () => {
            this.editPost(x, post);
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
    } else if (this.userDetail.id === userId && post.shared === 1) {
      const actionSheet = await this.actionSheetController.create({
        // mode: 'ios',
        // header: 'Albums',
        // cssClass: 'my-custom-class',
        buttons: [{
          text: this.deleteString,
          handler: () => {
            this.deletePost(x, post.id);
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
    } else {
      const actionSheet = await this.actionSheetController.create({
        buttons: [{
          text: this.reportString,
          icon: 'alert-circle-outline',
          handler: () => {
            this.reportPost(x, post.id);
          }
        }]
      });
      await actionSheet.present();
    }
    // if (this.postsArray[x].popUP) {
    //   this.postsArray[x].popUP = false;
    // } else {
    //   this.postsArray[x].popUP = true;
    //   this.popUpEdit.open();
    //   // this.postsArray[x].popUP.open();
    // }
    // setTimeout(() => {
    //   this.postsArray[x].popUP = false;
    // }, 8000);
  }
  reportPost(x, postId) {

  }
  // Post CRUD
  loadPost() {
    let offset = 0;
    if (this.postsArray.length !== 0) {
      offset = this.postsArray.length + 15;
    }
    this.apiService.getPostTimeline(offset, this.userDetail.id).subscribe((timelineRes: any) => {
      if (timelineRes.success === 1) {
        const postTemp = timelineRes.message;
        if (this.postsArray.length === 0) {
          this.postsArray = postTemp.content;
          this.limitReached = false;
          this.totalPost = postTemp.meta.total;
        } else {
          if (postTemp.content.length >= 1) {
            postTemp.content.forEach(post => {
              this.postsArray.push(post);
            });
          } else {
            this.limitReached = true;
          }
        }
        this.dontReloadMension();
        // let ss: any[] = [];
        // this.postsArray.forEach(e => {
        //   ss.push(e.id);
        // });
        // console.table(ss);
        console.log(this.postsArray);
      }
    });
  }

  creatPost() {
    // tslint:disable-next-line: prefer-const
    this.progressBarval = this.progressBarval + 1;
    let emptyVar;
    let mediaToSend;
    let mensionIds: number[] = [];
    this.tempPostContent = this.postContent;
    console.log('this.tempReplacement', this.tempReplacement);
    if (this.tempReplacement.length !== 0) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < this.tempReplacement.length; i++) {
        const xDx = this.tempReplacement[i];
        mensionIds.push(xDx.id);
        // tslint:disable-next-line: max-line-length
        this.tempPostContent = this.tempPostContent.replace(`@${xDx.replaced}`, ` <a href=\'${xDx.id}\' class=\'make_a_tag\'> ${xDx.name} </a> `);
      }
      // this.temprary = true;
      if (mensionIds.length === 0) {
        mensionIds = emptyVar;
      }
      const garbageSubString = this.occurrences(this.tempPostContent, '↵', false);
      for (let j = 0; j <= garbageSubString; j++) {
        this.tempPostContent = this.tempPostContent.replace('↵', ' ');
      }
    }

    let count = 0;
    let xDx: any[] = [];
    this.mediaArray.forEach(media => {
      this.apiService.uploadTimelineMedia(media.blob, this.userDetail.id, media.filename).subscribe((uploadRes: any) => {
        console.log('uploadRes', uploadRes);
        if (uploadRes.success === 1) {
          const temp = {
            originalImage: uploadRes.message.info.name,
            thumbnailImage: uploadRes.message.info.thumbname
          };
          xDx.push(temp);
          count += 1;
          console.log(count);
        }
      });
    });
    const uploadInt = setInterval(() => {
      console.log('count:: ', count, '  :this.mediaArray.length: ', this.mediaArray.length);
      if (count === this.mediaArray.length) {
        mediaToSend = xDx;
        this.apiService.savePostTimeline(emptyVar,
          this.userDetail.id, this.tempPostContent, emptyVar, mediaToSend, mensionIds).subscribe((post: any) => {
            if (post.success === 1) {
              this.tempPostContent = '';
              this.postContent = '';
              this.mediaArray = [];
              this.progressBarval = '0';
              this.postsArray = [];
              this.loadPost();
              this.checkMediaToUpload();
            }
          });
        clearInterval(uploadInt);
      }
    }, 100);
  }


  async editPost(x, post) {
    localStorage.setItem('postDataEdit', JSON.stringify(post));
    const modal = await this.modalController.create({
      component: EditPostComponent,
      componentProps: {
        custom_data: null
      }
    });
    modal.onDidDismiss().then((data) => {
      this.postsArray = [];
      this.loadPost();
      // this.closeModal(0);
      localStorage.removeItem('postDataEdit');
    });
    return await modal.present();

  }

  deletePost(x: number, postID: number) {
    this.apiService.deletePostTimeline(postID).subscribe((delet: any) => {
      if (delet.success === 1) {
        this.postsArray.splice(x, 1);
      }
    });
  }

  likePost(postId, xx) {
    if (this.postsArray[xx].liked === 1) {
      this.postsArray[xx].liked = 0;
      this.postsArray[xx].likes -= 1;
      this.apiService.unLikePost(this.userDetail.id, postId).subscribe((lyk: any) => {
        console.log(lyk);
      });
    } else {
      this.postsArray[xx].liked = 1;
      this.postsArray[xx].likes += 1;
      this.apiService.likePost(this.userDetail.id, postId).subscribe((lyk: any) => {
        console.log(lyk);
      });
    }
  }

  sharePost(postId, xx) {
    console.log(xx);
    this.apiService.sharePost(this.userDetail.id, postId).subscribe((shre: any) => {
      console.log(shre);
    });
  }
  async commentPost(feed, xx) {
    const feedDataComment = {
      postId: feed.id,
      userId: feed.user.id
    };
    localStorage.setItem('commentOnPost', JSON.stringify(feedDataComment));
    const modal = await this.modalController.create({
      component: CommentOnPostComponent,
      cssClass: 'comment_of_post',
    });
    modal.onDidDismiss().then((commentRes) => {
      localStorage.removeItem('commentOnPost');
      console.table(commentRes);
    });
    return await modal.present();
  }

  dismissAdminPost(postId) {
    console.log(postId);
  }

  private dontReloadMension(): void {
    setTimeout(() => {
      const urls: any = this.element.nativeElement.querySelectorAll('a');
      urls.forEach((url) => {
        url.addEventListener('click', (event) => {
          event.preventDefault();
          const textOfLink = event.target.innerText;
          console.log(textOfLink);
          const x = event.target.href.split(/[\s/]+/);
          this.router.navigate([`profile/${x[x.length - 1]}`]);
        }, false);

      });
    }, 1500);
  }
  mensioning(x) {
    if (x === 50) {
      this.showMensionPickUp = true;
    }
  }
  break(x) {
    this.showMensionPickUp = false;
    this.searchFriendsList = this.friendsList;
    this.moveFocus(this.postText);
  }
  moveFocus(nextElement) {
    nextElement.setFocus();
  }
  goToProfile(id) {
    this.router.navigate([`profile/${id}`]);
  }
  getTimeFromNow(createdDate) {
    return moment(createdDate).fromNow();
  }

  async openMultiMedia(fullAsserts, i) {
    const xDx = {
      asserts: fullAsserts,
      currentIndex: i
    };
    localStorage.setItem('mediaArray', JSON.stringify(xDx));
    const modal = await this.modalcontrolller.create({
      component: MultimediaViewComponent,
      // cssClass: 'sharePOP',
      componentProps: {
        // custom_id: this.shareValue
        swipeToClose: true
      }
    });
    return await modal.present();
  }

  // ------Branch Riddhi -----

  async attachMediaInPost() {
    const actionSheet = await this.actionSheetController.create({
      // header: 'Choo',
      buttons: [{
        text: 'Take Picture',
        role: 'destructive',
        handler: () => {
          // this.utilServ.showLoaderWait();
          this.captureImage();
        }
      }, {
        text: 'Take Video',
        handler: () => {
          // this.utilServ.showLoaderWait();
          this.captureVideo();
        }
      },
      {
        text: 'Gallery',
        handler: () => {
          // this.utilServ.showLoaderWait();
          this.userInputElement.click();
        }
      }]
    });
    await actionSheet.present();
  }

  captureImage() {
    this.nativeLib.captureImage(CameraSource.Camera);
    this.eventCustom.subscribe('imageReady', (imgData) => {
      if (imgData) {
        console.log('imgData', imgData);
        this.mediaArray.unshift(imgData);
        // this.utilServ.hideLoaderWait();
        // this.utilServ.hideLoaderWait();
        this.eventCustom.destroy('imageReady');
        this.checkMediaToUpload();
      }
    });
  }
  captureVideo() {
    this.nativeLib.captureVideo();
    this.eventCustom.subscribe('videoReady', (vidData) => {
      if (vidData) {
        console.log('vidData', vidData);
        this.mediaArray.unshift(vidData);
        // this.utilServ.hideLoaderWait();
        // this.utilServ.hideLoaderWait();
        this.eventCustom.destroy('videoReady');
        this.checkMediaToUpload();
      }
    });
  }
 
  pickImageVideoFromDevice(event) {
    console.log("myFile", event);
    const x = { mediaUrl: null, blob: null, filename: null, type: null };
    if (event.target.files && event.target.files[0]) {
      const myFile = event.target.files;

      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < myFile.length; i++) {
        x.blob = new Blob([myFile[i]], { type: myFile[i].type });
        x.mediaUrl = this.showImage(x.blob);
        x.filename = this.formateFileName(myFile[i].name);
        x.type = myFile[i].type;
        console.log(this.mediaArray)
        const innit = setInterval(() => {
          if (x.mediaUrl !== null) {
            this.mediaArray.unshift(x);
            this.checkMediaToUpload();
            clearInterval(innit);
          }
        }, 2);
      }

    }
  }
  showImage(image) {
    const imageURL = window.URL.createObjectURL(image);
    return this.sanitizer.bypassSecurityTrustUrl(imageURL);
  }
  formateFileName(filename) {
    let nameBeforeSpace: string = (filename.split(' '))[0];
    nameBeforeSpace = nameBeforeSpace.replace(/\s/g, '_');
    nameBeforeSpace = nameBeforeSpace.replace(/\./g, '_');
    const ext = (filename.split('.')).pop();
    const newFileName = this.userDetail.id + '_' + (this.utilServ.getTimeStamp()) + '_' + nameBeforeSpace + '.' + ext;
    return newFileName;
  }
  removeimg(i) {
    this.mediaArray.splice(i, 1);
    console.log(this.mediaArray);
    this.checkMediaToUpload();
  }
  checkMediaToUpload() {
    if (this.mediaArray.length > 0) {
      this.showPostButton = true;
    } else {
      this.showPostButton = false;
    }

  }
}