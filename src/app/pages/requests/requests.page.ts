import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Socket } from 'ngx-socket-io';
import { AlertController } from '@ionic/angular';
import * as moment from 'moment';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.page.html',
  styleUrls: ['./requests.page.scss'],
})
export class RequestsPage implements OnInit {
  // Strings
  requestsString = 'Requests';
  noRequestsString = 'No Requests';
  friendReqString = 'Friend Request';
  groupChatReqString = 'Group Chat Request';
  teamReqString = 'Team Request';
  searchString = 'Search';
  teamNameString = 'Team Name';
  acceptString = 'Accept';
  deleteString = 'Delete';
  cancelString = 'Cancel';
  okString = 'Ok';
  acceptFrndReqString = 'Are you sure you want to accept friend request?';
  deleteFrndReqString = 'Are you sure you want to reject the friend request?';
  acceptTeamReqString = 'Are you sure you want to accept team request?';
  deleteTeamReqString = 'Are you sure you want to reject the team request?';
  acceptGrpReqString = 'Are you sure you want to accept group request?';
  deleteGrpReqString = 'Are you sure you want to reject the group request?';
  noFriendReqString = 'No friend request';
  noTeamReqString = 'No team request';
  noGrpReqString = 'No group request';

  searchFriendsTerm;
  searchTeamTerm;
  searchGrpChatTerm;

  noRequest = true;
  environment: any;

  data: any;
  usersdetail: any;

  friendRequests: any[] = [];
  teamRequests: any[] = [];
  groupChatRequests: any[] = [];

  friendReqList: any[] = [];
  teamReqList: any[] = [];
  groupReqList: any[] = [];

  constructor(
    private apiService: ApiService,
    private actRouter: ActivatedRoute,
    private utilServ: GenralUtilsService,
    private socket: Socket,
    private alertController: AlertController) {
    this.actRouter.params.subscribe(data => {
      this.usersdetail = JSON.parse(this.utilServ.getUserDetails());
      this.getFriendReq();
      this.getTeamReq();
      this.getGrpChatReq();

      // Friend Request
      this.socket.on('send-friend-request', (res) => {
        // localStorage.setItem('getNotificationAgain', 'true');
        this.getFriendReq();
      });
      this.socket.on('cancel-friend-request', (res) => {
        this.getFriendReq();
      });

      // Team Request
      this.socket.on('invite-team-member', (res) => {
        this.getTeamReq();
      });
      this.socket.on('remove-team-user-invite', (res) => {
        this.getTeamReq();
      });

      // Group chat Request
      this.socket.on('invite-groupchat-member', (res) => {
        this.getGrpChatReq();
      });
      this.socket.on('remove-groupchat-user-invite', (res) => {
        this.getGrpChatReq();
      });
    });
  }

  ngOnInit() {
    this.environment = environment;

  }

  getFriendReq() {
    this.apiService.getFriendRequest(this.usersdetail.id).subscribe((res: any) => {
      console.log(res);
      if (res.message.friendRequestData) {
        this.friendReqList = res.message.friendRequestData;
        this.friendRequests = this.friendReqList;
        this.checkReqExist();
      }
    });
  }
  getTeamReq() {
    this.apiService.getTeamRequest(this.usersdetail.id).subscribe((res: any) => {
      if (res) {
        this.teamReqList = res.message;
        this.teamRequests = this.teamReqList;

        this.checkReqExist();
      }
    });
  }
  getGrpChatReq() {
    this.apiService.getGroupchatRequest(this.usersdetail.id).subscribe((res: any) => {
      if (res) {
        this.groupReqList = res.message;
        this.groupChatRequests = this.groupReqList;
        this.checkReqExist();
      }
    });
  }

  async acceptFrndReq(itemId, n) {
    const alert = await this.alertController.create({
      message: this.acceptFrndReqString,
      buttons: [
        {
          text: this.cancelString,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: this.okString,
          handler: () => {
            this.data = {
              user_id: this.usersdetail.id,
              friend_id: itemId
            };
            this.socket.emit('accept-friend', this.data);
            this.friendRequests.splice(n, 1);
            this.checkReqExist();
          }
        }
      ]
    });
    await alert.present();
  }
  async deleteFrndReq(itemId, n) {
    const alert = await this.alertController.create({
      message: this.deleteFrndReqString,
      buttons: [
        {
          text: this.cancelString,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: this.okString,
          handler: () => {
            this.data = {
              user_id: this.usersdetail.id,
              friend_id: itemId
            };

            this.socket.emit('reject-friend', this.data);
            this.friendRequests.splice(n, 1);
            this.checkReqExist();

          }
        }
      ]
    });
    await alert.present();
  }

  async acceptTeamReq(teamData, n) {
    const alert = await this.alertController.create({
      message: this.acceptTeamReqString,
      buttons: [
        {
          text: this.cancelString,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: this.okString,
          handler: () => {
            this.apiService.acceptTeamInviteRequest(this.usersdetail.id, teamData.teamid).subscribe((res: any) => {
              const grpCreatData = {
                message: {
                  group_id: teamData.teamid,
                  group_type: 1,
                  msg_type: 3,
                  content: '',
                  media: '',
                  userinfo: { from_id: this.usersdetail.id, name: this.usersdetail.first_name }
                }
              };
              this.socket.emit('send-group-userchat-status', grpCreatData);
              this.teamRequests.splice(n, 1);
              this.checkReqExist();

            });
          }
        }
      ]
    });
    await alert.present();
  }
  async deleteTeamReq(teamData, n) {
    const alert = await this.alertController.create({
      message: this.deleteTeamReqString,
      buttons: [
        {
          text: this.cancelString,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: this.okString,
          handler: () => {
            this.apiService.rejectTeamInviteRequest(this.usersdetail.id, teamData.teamid).subscribe((res: any) => {
              this.data = {
                user_id: this.usersdetail.id,
                teamid: teamData.teamid,
                manager: teamData.masteradminid
              };

              this.socket.emit('reject-team-request', this.data);
              this.teamRequests.splice(n, 1);
              this.checkReqExist();
            });
          }
        }
      ]
    });
    await alert.present();
  }

  async acceptGrpReq(itemId, itemGrpMaster, n) {
    const alert = await this.alertController.create({
      message: this.acceptGrpReqString,
      buttons: [
        {
          text: this.cancelString,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: this.okString,
          handler: () => {
            this.apiService.acceptGroupchatInviteRequest(this.usersdetail.id, itemId).subscribe((res: any) => {
              this.data = {
                user_id: this.usersdetail.id,
                group_id: itemId,
                manager: itemGrpMaster
              };
              this.socket.emit('accept-groupchat-join', this.data);
              const grpCreatData = {
                message: {
                  group_id: itemId,
                  group_type: 2,
                  msg_type: 3,
                  content: '',
                  media: '',
                  userinfo: { from_id: this.usersdetail.id, name: this.usersdetail.first_name }
                }
              };
              this.socket.emit('send-group-userchat-status', grpCreatData);
              this.groupChatRequests.splice(n, 1);
              this.checkReqExist();
            });
          }
        }
      ]
    });
    await alert.present();
  }
  async deleteGrpReq(itemId, itemGrpMaster, n) {
    const alert = await this.alertController.create({
      message: this.deleteGrpReqString,
      buttons: [
        {
          text: this.cancelString,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: this.okString,
          handler: () => {
            this.apiService.rejectGroupchatInviteRequest(this.usersdetail.id, itemId).subscribe((res: any) => {
              this.data = {
                user_id: this.usersdetail.id,
                teamid: itemId,
                manager: itemGrpMaster
              };

              this.socket.emit('reject-groupchat-request', this.data);
              this.groupChatRequests.splice(n, 1);
              this.checkReqExist();
            });
          }
        }
      ]
    });
    await alert.present();
  }

  friendDetail() {
    // Nav to Friend Profile
  }

  // Search on direct chat
  searchFrndReq(x) {
    this.friendRequests = this.filterFrndReq(this.friendReqList, x || null);
  }
  searchTeamReq(x) {
    this.teamRequests = this.filterTeamReq(this.teamReqList, x || null);
  }
  searchGrpReq(x) {
    this.groupChatRequests = this.filterGrpReq(this.groupReqList, x || null);
  }
  filterFrndReq(items: any[], terms: string): any[] {
    if (!items) { return []; }
    if (!terms) { return items; }
    terms = terms.toLowerCase();
    return items.filter(it => {
      return it.sender_name.toLowerCase().includes(terms);
    });
  }
  filterTeamReq(items: any[], terms: string): any[] {
    if (!items) { return []; }
    if (!terms) { return items; }
    terms = terms.toLowerCase();
    return items.filter(it => {
      return it.team_name.toLowerCase().includes(terms);
    });
  }
  filterGrpReq(items: any[], terms: string): any[] {
    if (!items) { return []; }
    if (!terms) { return items; }
    terms = terms.toLowerCase();
    return items.filter(it => {
      return it.group_name.toLowerCase().includes(terms);
    });
  }

  getTimeFromNow(dates) {
    return moment(dates).fromNow(true);
  }

  checkReqExist() {
    setTimeout(() => {
      if (this.teamRequests.length > 0 || this.friendRequests.length > 0 || this.groupChatRequests.length > 0) {
        this.noRequest = false;
      } else {
        this.noRequest = true;
      }
    }, 200);
  }

}
