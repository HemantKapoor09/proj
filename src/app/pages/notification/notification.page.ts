import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { environment } from 'src/environments/environment';
import { ModalController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Socket } from 'ngx-socket-io';
import { once } from 'process';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {

  limitOfResults = 20;
  skipFirst = 0;
  userDetail: any;
  noNotification = true;
  notificationList: any = null;
  totalNotification: any;
  environment: any;
  limitReached = false;

  // String
  notifications = 'Notifications';
  noNotificationsString = 'No Notifications';
  clearAllNotifiString = 'Are you sure you want to delete all notifications?';
  cancelString = 'Cancel';
  okString = 'Ok';
  allNotifiDelString = 'All Notifications are deleted';
  endOfSearchResultString = 'End of search results';
  deleteString = 'Delete';
  deleteSingleNotiString = 'Are you sure you want to delete this notification?';

  notifiTypeTeamCreate = 'team-create';
  notifiTypeTeamInvite = 'team-invite';
  notifiTypeFriendReq = 'friend-request';
  notifiTypeEventNotifi = 'event-notification';
  notifiTypeEventModify = 'event-notification-modify';
  notifiTypeEventDelete = 'event-notification-delete';
  notifiTypeFrndReqAccept = 'friend-request-accept';
  notifiTypeGrpChatInvite = 'groupchat-invite';
  notifiTypeMentionInPost = 'mention-notification';
  notifiTypeLikePost = 'like-notification';
  notifiTypeCommentPost = 'comment-notification';

  notifiTypeTeamCreateString = 'has invited you to join a team';
  notifiTypeTeamInviteString = 'has invited you to join a team';
  notifiTypeFriendReqString = 'sent you a friend request';
  notifiTypeEventNotifiString = 'has created an event ';
  notifiTypeEventModifyString = 'has changed an event';
  notifiTypeEventDeleteString = 'has deleted an event';
  notifiTypeFrndReqAcceptString = 'accepted your friend request';
  notifiTypeGrpChatInviteString = 'has invited you to join a group chat';
  notifiTypeMentionInPostString = 'has mentioned you in a post';
  notifiTypeLikePostString = 'liked your post';
  notifiTypeCommentPostString = 'commented on your post';
  defaultString = 'sent you something';
  inaATeamString = ' in a team';

  constructor(
    private actRouter: ActivatedRoute,
    private apiService: ApiService,
    private alertController: AlertController,
    private router: Router,
    private socket: Socket,
    private utilServ: GenralUtilsService) {
    this.actRouter.queryParams.subscribe(() => {
      this.notificationList = null;

      this.userDetail = JSON.parse(this.utilServ.getUserDetails());
      this.getNotifications();
      this.utilServ.checkUserExists();
    });
  }

  ngOnInit() {
    this.environment = environment;
  }
  async deleteAllNotifications() {
    const confirm = await this.alertController.create({
      header: this.deleteString + '?',
      message: this.clearAllNotifiString,
      buttons: [{
        text: this.cancelString,
        role: 'cancel'
      },
      {
        text: this.okString,
        handler: () => {
          this.apiService.clearAllNotification(this.userDetail.id).pipe().subscribe(async (res: any) => {
            console.log(res.success);
            if (res.success === 1) {
              this.notificationList.splice(0, this.notificationList.length);
              this.utilServ.okButtonMessageAlert(this.allNotifiDelString);
              this.checkNotificationsExist();
            }
          });
        }
      }
      ]
    });
    confirm.present();
  }
  async deleteNotification(itemId, n) {
    const confirm = await this.alertController.create({
      header: this.deleteString + '?',
      cssClass: 'buttonCss',
      message: this.deleteSingleNotiString,
      buttons: [{
        text: this.cancelString,
        role: 'cancel',
      },
      {
        text: this.okString,
        handler: () => {
          this.apiService.deleteNotification(itemId).pipe().subscribe((res: any) => {
            if (res.success === 1) {
              this.notificationList.splice(n, 1);
              this.checkNotificationsExist();
            }
          });
        }
      }
      ]
    });
    confirm.present();
  }
  getNotifications() {
    this.apiService.getNotificationMore(this.userDetail.id, this.limitOfResults, this.skipFirst).pipe().subscribe((res: any) => {
      if (res.success === 1) {
        this.totalNotification = res.message.meta.total;
        if (this.notificationList) {
          const temp = res.message.content;
          temp.forEach(e => {
            const xd: any = this.getNotificationType(e.notification_type, e.from_id, e.eventname);
            e.notification_type = xd.displayText;
            e.route = xd.route;
            this.notificationList.push(e);
          });
        } else {
          this.notificationList = res.message.content;
          this.notificationList.forEach(e => {
            const xd: any = this.getNotificationType(e.notification_type, e.from_id, e.eventname);
            e.notification_type = xd.displayText;
            e.route = xd.route;
          });
          console.log(res.message);
        }
      }
    });
    this.checkNotificationsExist();
  }
  getTimeFromNow(dates) {
    return moment(dates).fromNow();
  }

  checkNotificationsExist() {
    setTimeout(() => {
      if (this.notificationList.length > 0) {
        this.noNotification = false;
      } else {
        this.noNotification = true;
      }
    }, 200);
  }
  loadMore(e) {
    if (this.notificationList) {
      console.log(this.totalNotification, this.notificationList.length)
      if (this.totalNotification === this.notificationList.length) {
        this.limitReached = true;
      } else {
        this.skipFirst += 20;
        this.getNotifications();
      }
    }
    setTimeout(() => {
      e.target.complete();
    }, 500);
  }
  routeTo(x) {
    this.router.navigate([x]);
  }
  getNotificationType(notiType, fromId, eventName) {
    switch (notiType) {
      case this.notifiTypeTeamCreate:
        {
          return {
            displayText: this.notifiTypeTeamCreateString,
            route: '/requests'
          };
        }
      case this.notifiTypeTeamInvite:
        {
          return {
            displayText: this.notifiTypeTeamInviteString,
            route: '/requests'
          };
        }
      case this.notifiTypeFriendReq:
        {
          return {
            displayText: this.notifiTypeFriendReqString,
            route: '/requests'
          };
        }
      case this.notifiTypeEventNotifi:
        {
          return {
            displayText: this.notifiTypeEventNotifiString + JSON.stringify(eventName) + this.inaATeamString,
            route: '/events'
          };
        }
      case this.notifiTypeEventModify:
        {
          return {
            displayText: this.notifiTypeEventModifyString,
            route: '/events'
          };
        }
      case this.notifiTypeEventDelete:
        {
          return {
            displayText: this.notifiTypeEventDeleteString,
            route: '/events'
          };
        }
      case this.notifiTypeFrndReqAccept:
        {
          return {
            displayText: this.notifiTypeFrndReqAcceptString,
            route: '/profile/' + fromId
          };
        }
      case this.notifiTypeGrpChatInvite:
        {
          return {
            displayText: this.notifiTypeGrpChatInviteString,
            route: '/requests'
          };
        }
      case this.notifiTypeMentionInPost:
        {
          return {
            displayText: this.notifiTypeMentionInPostString,
            route: '/timeline'
          };
        }
      case this.notifiTypeLikePost:
        {
          return {
            displayText: this.notifiTypeLikePostString,
            route: '/timeline'
          };
        }
      case this.notifiTypeCommentPost:
        {
          return {
            displayText: this.notifiTypeCommentPostString,
            route: '/timeline'
          };
        }
      default:
        return this.defaultString;
    }
  }
}
