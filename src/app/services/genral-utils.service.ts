import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { ToastController, NavController, AlertController, Platform, LoadingController } from '@ionic/angular';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { Location, JsonPipe } from '@angular/common';
// import { Globalization } from '@ionic-native/globalization/ngx';
import { ApiService } from './api.service';
import { EventsCustomService } from './events-custom.service';
import { BehaviorSubject } from 'rxjs';
import * as deepEqual from 'deep-equal';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class GenralUtilsService {
  // everythingUpFlag = new BehaviorSubject(false);
  loaderToShow: Promise<void>;

  languageData;
  langLibrary = null;
  langSetupFLag = false;

  introStatus: number;

  userDetail: any = null;

  friendsList = null;
  favSports = [];
  teamData: any = null;
  sportsList: any;
  countryList: any;

  constructor(
    private network: Network,
    private toast: ToastController,
    // private globalization: Globalization,
    private loadingController: LoadingController,
    private platform: Platform,
    private alertController: AlertController,
    private toastController: ToastController,
    private location: Location,
    private router: Router,
    private keyboard: Keyboard,
    private eventCustom: EventsCustomService,
    private navCtrl: NavController,
    private apiService: ApiService,
    private actRouter: ActivatedRoute) {
    this.userDetail = JSON.parse(localStorage.getItem('userdetail'));
    this.apiService.connectFuntion();
    this.actRouter.queryParams.subscribe(() => {
      this.userDetail = JSON.parse(localStorage.getItem('userdetail'));
    });
    this.setUpUser();

  }

  checkUserExists() {
    if ((!this.userDetail || this.userDetail == null) && this.langSetupFLag === true) {
      this.navLogin();
    } else {
      console.log('hi log CheckUsre');
    }
  }

  checkAlredyLoggedIn() {
    if (this.userDetail != null) {
      return true;
    } else {
      return false;
    }
  }


  navLogin() {
    this.navCtrl.navigateRoot(['/login']);
  }
  navSignup() {
    this.navCtrl.navigateForward(['/signup']);
  }
  navTimeline() {
    this.setUpUser();
    this.apiService.connectFuntion();
    this.languageSetup();
    this.navCtrl.navigateRoot(['/timeline']);
  }
  navForgetPass() {
    this.navCtrl.navigateForward(['/login/forget-pass']);
  }
  navSetting() {
    this.navCtrl.navigateForward(['/settings']);
    // this.navCtrl.consumeTransition('/se',screenLeft)
  }
  navChatwithId(id) {
    this.router.navigate([`/chat/direct-chat/${id}`]);
  }
  navMainChat() {
    this.router.navigate(['/chat']);
  }

  navGroupChatwithId(id) {
    this.router.navigate([`/chat/group-chat/${id}`]);
  }


  checkBasicElseRelodApp() {
    if (!this.userDetail && this.langSetupFLag === false) {
      this.apiService.connectFuntion();
      this.setUpUser();
      window.location.reload();
      alert('relod');
    }
  }
  setSportsList() {
    if (!this.sportsList) {
      this.apiService.connectFuntion();
      this.apiService.getSportLists().subscribe((res: any) => {
        this.sportsList = res.message;
        this.getSportsList();
        return true;
      }, error => {
        this.networkError();
      });
    }
  }
  getSportsList() {
    if (this.sportsList) {
      return this.sportsList;
    } else {
      this.setSportsList();
    }
  }
  getCountryList() {
    if (!this.countryList) {
      this.apiService.connectFuntion();
      this.apiService.getCountryList().subscribe((res: any) => {
        this.countryList = res.message;
        return this.countryList;
      }, error => {
        this.networkError();
      });
    } else {
      return this.countryList;
    }
  }
  exitKeyBoard() {
    this.keyboard.hide();
  }
  showKeyBoard() {
    this.keyboard.show();
  }
  async okButtonMessageAlert(data) {
    const alert = await this.alertController.create({
      message: data,
      buttons: ['OK']
    });
    await alert.present();
  }

  async noNetworkAlert() {
    const alert = await this.alertController.create({
      header: 'Oops!!!',
      message: 'Something went wrong ! Please check your Network connection',
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 4000
    });
    toast.present();
  }

  setUpUser() {
    this.apiService.connectFuntion();
    this.introStatus = Number(localStorage.getItem('intro_status'));
    this.userDetail = JSON.parse(localStorage.getItem('userdetail'));
    if (this.userDetail) {
      this.eventCustom.publish('userDetail', this.userDetail);
      localStorage.setItem('profileLang', this.userDetail.language);
    }

    if (this.langSetupFLag === false) { this.languageSetup(); }

    // if (this.langSetupFLag === true && this.userDetail) {
    //   this.everythingUpFlag.next(true);
    // }
    this.apiService.reloadApp();
  }

  getUserDetails() {
    this.apiService.connectFuntion();
    return JSON.stringify(this.userDetail);
  }

  userAddonDetails() {
    if (!this.friendsList && this.userDetail) {
      this.apiService.getFriendsList(this.userDetail.id).subscribe((res: any) => {
        this.apiService.connectFuntion();
        this.friendsList = res.message;
      }, error => {
        this.networkError();
      });
    }

    setTimeout(() => {
      if (this.favSports.length <= 1) {
        this.apiService.getFollowingSportsData(this.userDetail.id).subscribe((res: any) => {
          this.apiService.connectFuntion();
          const followSports = res.message;
          // tslint:disable-next-line: prefer-for-of
          for (let v = 0; v < followSports.length; v++) {
            if (followSports[v].sport_id !== null) {
              this.favSports.push(followSports[v].sport_id);
            }
          }
        }, error => {
          this.networkError();
        });
      }
    }, 10000);
  }

  getIntroStatus() {
    return this.introStatus;
  }

  setIntroStatus(intro) {
    localStorage.setItem('intro_status', JSON.stringify(this.introStatus));
    this.introStatus = intro;
  }

  languageSetup() {
    this.apiService.connectFuntion();
    let lang: string; // Device Detected Language
    let deviceLang;
    if (this.platform.is('hybrid')) {
      // this.globalization.getPreferredLanguage().then(langInstall => {
      //   deviceLang = langInstall.value.split('-');
      //   deviceLang = deviceLang[0];
      //   switch (deviceLang) {

      //     case 'hi': {
      //       lang = 'हिंदू';
      //       break;
      //     }
      //     case 'en': {
      //       lang = 'English';
      //       break;
      //     }
      //     case 'tr': {
      //       lang = 'türk';
      //       break;
      //     }
      //     case 'fr': {
      //       lang = 'français';
      //       break;
      //     }
      //     case 'fa': {
      //       lang = 'farsça';
      //       break;
      //     }
      //     case 'Ja': {
      //       lang = 'japonca';
      //       break;
      //     }
      //     case 'el': {
      //       lang = 'yunanca';
      //       break;
      //     }
      //     case 'ko': {
      //       lang = 'korece';
      //       break;
      //     }
      //     case 'es': {
      //       lang = 'español';
      //       break;
      //     }
      //     case 'it': {
      //       lang = 'italiano';
      //       break;
      //     }
      //     case 'ru': {
      //       lang = 'pусский';
      //       break;
      //     }
      //     case 'nl': {
      //       lang = 'nederlands';
      //       break;
      //     }
      //     case 'ar': {
      //       lang = 'عربى';
      //       break;
      //     }
      //     case 'hu': {
      //       lang = 'magyar nyelv';
      //       break;
      //     }
      //     case 'he': {
      //       lang = 'עִברִית';
      //       break;
      //     }
      //     case 'fil': {
      //       lang = 'filipino';
      //       break;
      //     }
      //     case 'el': {
      //       lang = 'eλληνικά';
      //       break;
      //     }
      //     case 'pa': {
      //       lang = 'ਪੰਜਾਬੀ';
      //       break;
      //     }
      //     case 'ka': {
      //       lang = 'ქართული';
      //       break;
      //     }
      //     case 'it': {
      //       lang = 'italyanca';
      //       break;
      //     }
      //     case 'ru': {
      //       lang = 'Русский язык';
      //       break;
      //     }
      //     case 'az': {
      //       lang = 'Азәрбајҹан дили';
      //       break;
      //     }
      //     case 'zh': {
      //       lang = '中文';
      //       break;
      //     }
      //     case 'sq': {
      //       lang = 'shqip';
      //       break;
      //     }
      //     case 'bs': {
      //       lang = 'bosanski';
      //       break;
      //     }
      //     case 'bg': {
      //       lang = 'Абонирай се';
      //       break;
      //     }
      //     case 'cs': {
      //       lang = 'čeština';
      //       break;
      //     }
      //     case 'fi': {
      //       lang = 'suomi';
      //       break;
      //     }
      //     case 'nb': {
      //       lang = 'norsk';
      //       break;
      //     }
      //     case 'tk': {
      //       lang = 'түркmенче';
      //       break;
      //     }
      //     case 'ro': {
      //       lang = 'limba română';
      //       break;
      //     }
      //     case 'uz': {
      //       lang = 'ozbekçe';
      //       break;
      //     }
      //     case 'kk': {
      //       lang = 'Қазақ тілі';
      //       break;
      //     }
      //     case 'hr': {
      //       lang = 'hrvatski';
      //       break;
      //     }
      //     case 'ms': {
      //       lang = 'bahasa melayu';
      //       break;
      //     }
      //     case 'id': {
      //       lang = 'endonezce';
      //       break;
      //     }
      //     case 'pl': {
      //       lang = 'język polski';
      //       break;
      //     }
      //     case 'de-CH': {
      //       lang = 'schwyzerdütsch';
      //       break;
      //     }
      //     case 'ps': {
      //       lang = 'پښتو';
      //       break;
      //     }
      //     case 'be': {
      //       lang = 'Беларуская мова';
      //       break;
      //     }
      //     case 'et': {
      //       lang = 'eesti keel';
      //       break;
      //     }
      //     case 'is': {
      //       lang = 'Íslenska';
      //       break;
      //     }
      //     case 'lv': {
      //       lang = 'lietuvių kalba';
      //       break;
      //     }
      //     case 'ky': {
      //       lang = 'قىرعىز تىلى';
      //       break;
      //     }
      //     case 'mo': {
      //       lang = 'лимба молдовеняскэ';
      //       break;
      //     }
      //     case 'sk': {
      //       lang = 'slovenčina';
      //       break;
      //     }
      //     case 'sl': {
      //       lang = 'slovenščina';
      //       break;
      //     }
      //     case 'vi': {
      //       lang = 'tiếng việt';
      //       break;
      //     }

      //     default: {
      //       lang = 'english';
      //       break;
      //     }
      //   }
      // });
    }
    localStorage.setItem('deviceLanguage', lang ? lang : 'english');
    this.initLang();
  }

  initLang() {
    // Prompt IF change Fun here
    this.apiService.connectFuntion();
    const langDevice = (localStorage.getItem('deviceLanguage') || 'english');
    const profileLang = (localStorage.getItem('profileLang') || 'english');
    this.langLibrary = JSON.parse(localStorage.getItem('languageSetup'));
    if (this.langLibrary !== null) {
      this.langSetupFLag = true;
      this.apiService.reloadApp();
    }

    if (this.langSetupFLag === false) {
      this.apiService.connectFuntion();
      console.log('calling api for lang');
      this.apiService.languageFile(profileLang).subscribe((res: any) => {
        this.apiService.connectFuntion();
        if (res.success === 1) {
          this.langLibrary = res.message.Sheet1;
          localStorage.setItem('languageSetup', JSON.stringify(this.langLibrary));
          this.langSetupFLag = true;
        }
      }, error => {
        this.networkError();
      });
    }
  }

  getLangByCode(codeToFind: string): any {
    if (this.langLibrary) {
      const xDx = this.langLibrary.filter(d => d.code === codeToFind);
      if (xDx[0]) {
        return (xDx[0]).response;
      }
    } else {
      this.initLang();
    }
  }


  formatDateMax() {
    const dd = new Date();
    let day = '' + dd.getDate();
    let month = '' + (dd.getMonth() + 1);
    const year = (dd.getFullYear() + 100);
    if (month.length < 2) {
      month = '0' + month;
    }
    if (day.length < 2) {
      day = '0' + day;
    }
    return [year, month, day].join('-');
  }
  formatDateMin() {
    const dd = new Date();
    let day = '' + dd.getDate();
    let month = '' + (dd.getMonth() + 1);
    const year = (dd.getFullYear() - 13);
    if (month.length < 2) {
      month = '0' + month;
    }
    if (day.length < 2) {
      day = '0' + day;
    }
    return [year, month, day].join('-');
  }

  minToday() {
    const dd = new Date();
    let day = '' + dd.getDate();
    let month = '' + (dd.getMonth() + 1);
    const year = dd.getFullYear();
    if (month.length < 2) {
      month = '0' + month;
    }
    if (day.length < 2) {
      day = '0' + day;
    }
    return [year, month, day].join('-');
  }
  max5yeras() {
    const dd = new Date();
    let day = '' + dd.getDate();
    let month = '' + (dd.getMonth() + 1);
    const year = dd.getFullYear();
    if (month.length < 2) {
      month = '0' + month;
    }
    if (day.length < 2) {
      day = '0' + day;
    }
    return [(year + 5), month, day].join('-');
  }
  getYYYY_MM_DD(x) {
    if (x) {
      x.split('T', 1);
      const pubdate = new Date(x).toISOString().
        replace(/T/, ' ').      // replace T with a space
        replace(/\..+/, '');
      return (pubdate.split(' ', 1)[0]);
    }
    // x.split('T', 1);
    // return String(x[0]);

    // return moment(x).format('YYYY-MM-DD');
  }
  getTimeStamp() {
    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    const time = today.getHours() + '-' + today.getMinutes() + '-' + today.getSeconds();
    const dateTime = date + '-' + time;
    return dateTime;
  }

  compareObjectsEqual(obj1: object, obj2: object): boolean {
    return (deepEqual(obj1, obj2));
  }

  formatString(strng) {
    if (strng) {
      const len = strng.length;
      for (let x = 0; x <= len / 2; x++) {
        for (let j = 0; j <= len; j++) {
          if (strng.substr(0, 1) === '\n') {
            strng = strng.slice(1);
          }
        }
        for (let j = 0; j <= len; j++) {
          if (strng.substr(0, 1) === ' ') {
            strng = strng.slice(1);
          }
        }

        for (let i = 0; i <= len; i++) {
          if (strng.substr(-1, len) === '\n') {
            strng = strng.slice(0, -1);
          }
        }
        for (let i = 0; i <= len; i++) {
          if (strng.substr(-1, len) === ' ') {
            strng = strng.slice(0, -1);
          }
        }
      }
      return strng;
    }
  }
  showLoaderWait() {
    this.loaderToShow = this.loadingController.create({
      message: this.getLangByCode('post_error2')
    }).then((res) => {
      res.present();
      res.onDidDismiss().then((dis) => {
        this.apiService.connectFuntion();
      });
    });
  }
  hideLoaderWait() {
    setTimeout(() => {
      this.loadingController.dismiss();
      console.log('Outside Won');
      this.apiService.connectFuntion();
    }, 100);
  }

  hideLoaderWaitAMin() {
    setTimeout(() => {
      this.loadingController.dismiss();
      console.log('Outside Won');
      this.apiService.connectFuntion();
    }, 500);

  }
  //   search(searchTerm, searchArry, whatToFind) {
  //   return this.filter(searchArry, searchTerm, whatToFind || null);
  //   }

  //  filter(items: any[], terms: string , whatToFind : any): any[] {


  //   if (!whatToFind) { return []; }
  //    if (!items) { return []; }
  //    if (!terms) { return items; }
  //    terms = terms.toLowerCase();
  //    whatToFind = JSON.stringify(whatToFind);
  //    var ty = "team_name";
  //    return items.filter(it => {
  //      console.log("it", it.whatToFind);

  //   //  return it.whatToFind.toLowerCase().includes(terms);

  //    });
  //  }
  networkError() {
   
    console.log('error in network');
    localStorage.setItem('online', 'false');
    // this.navLogin();
  }
}
