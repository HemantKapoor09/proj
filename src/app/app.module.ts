import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, ModalController, PopoverController, IonReorderGroup } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Network } from '@ionic-native/network/ngx';
import { MediaCapture } from '@ionic-native/media-capture/ngx';
import { Calendar } from '@ionic-native/calendar/ngx';
import { RecaptchaModule } from 'ng-recaptcha';
import { File, DirectoryEntry } from '@ionic-native/file/ngx';
import { SocialSharing } from '@ionic-native/social-Sharing/ngx';


import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuTrigger } from '@angular/material/menu';

// import { MatRadioButton, MatRadioChange, MatRadioGroup, MatRadioModule } from '@angular/material/radio';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GenralUtilsService } from './services/genral-utils.service';
import { EventsCustomService } from './services/events-custom.service';

import { ViewTeamEventComponent } from './components/view-team-event/view-team-event.component';
import { AddTeamEventComponent } from './components/add-team-event/add-team-event.component';
import { AboutTeamComponent } from './components/about-team/about-team.component';
import { EditTeamEventComponent } from './components/edit-team-event/edit-team-event.component';
import { AddTeamMemberComponent } from './components/add-team-member/add-team-member.component';
import { ScoutToolFilterComponent } from './components/scout-tool-filter/scout-tool-filter.component';
import { SelectCountryComponent } from './components/select-country/select-country.component';
import { SelectSportsOrLagacyComponent } from './components/select-sports-or-lagacy/select-sports-or-lagacy.component';
import { SearchInAppComponent } from './components/search-in-app/search-in-app.component';
import { ProfilePortfolioComponent } from './components/profile-portfolio/profile-portfolio.component';
import { CreateGroupChatComponent } from './components/create-group-chat/create-group-chat.component';
import { AddGroupChatMembersComponent } from './components/add-group-chat-members/add-group-chat-members.component';
import { SharePopComponent } from './components/share-pop/share-pop.component';
import { MentionModule } from 'angular-mentions';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AddSectionComponent } from './components/profile-portfolio/add-section/add-section.component';
import { AboutGroupChatComponent } from './components/about-group-chat/about-group-chat.component';
import { PositionForSportsComponent } from './components/select-sports-or-lagacy/position-for-sports/position-for-sports.component';
import { CommentOnPostComponent } from './components/comment-on-post/comment-on-post.component';
import { EditPostComponent } from './components/edit-post/edit-post.component';
import { MultimediaViewComponent } from './components/multimedia-view/multimedia-view.component';
import { GuardianDocUploaderComponent } from './components/guardian-doc-uploader/guardian-doc-uploader.component';
import { ScoutDocUploaderComponent } from './components/scout-doc-uploader/scout-doc-uploader.component';
import { SplashComponent } from './components/splash/splash.component';



// API for socket connection
const socketConfig: SocketIoConfig = { url: 'https://tribation.com/', options: {}, };

@NgModule({
  declarations: [
    AppComponent,
    SharePopComponent,
    ViewTeamEventComponent,
    AddTeamEventComponent,
    AboutTeamComponent,
    EditTeamEventComponent,
    AddTeamMemberComponent,
    ScoutToolFilterComponent,
    SelectCountryComponent,
    SelectSportsOrLagacyComponent,
    SearchInAppComponent,
    ProfilePortfolioComponent,
    CreateGroupChatComponent,
    AddGroupChatMembersComponent,
    AddSectionComponent,
    AboutGroupChatComponent,
    PositionForSportsComponent,
    CommentOnPostComponent,
    MultimediaViewComponent,
    EditPostComponent,
    GuardianDocUploaderComponent,
    ScoutDocUploaderComponent,
    SplashComponent,
  ],
  entryComponents: [
    SharePopComponent,
    ViewTeamEventComponent,
    AddTeamEventComponent,
    AboutTeamComponent,
    EditTeamEventComponent,
    AddTeamMemberComponent,
    ScoutToolFilterComponent,
    SelectSportsOrLagacyComponent,
    SelectCountryComponent,
    SearchInAppComponent,
    ProfilePortfolioComponent,
    IonReorderGroup,
    AddGroupChatMembersComponent,
    CreateGroupChatComponent,
    AddSectionComponent,
    AboutGroupChatComponent,
    PositionForSportsComponent,
    MultimediaViewComponent,
    CommentOnPostComponent,
    EditPostComponent,
    GuardianDocUploaderComponent,
    ScoutDocUploaderComponent,
    SplashComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    BrowserAnimationsModule,
    SocketIoModule.forRoot(socketConfig),
    RecaptchaModule,
    MatCardModule,
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    // MatRadioModule,
    HttpClientModule,
    FormsModule,
    MentionModule,
    ReactiveFormsModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    GenralUtilsService,
    InAppBrowser,
    EventsCustomService,
    AndroidPermissions,
    Network,
    Keyboard,
    File,
    SocialSharing,
    Calendar,
    MediaCapture,
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
