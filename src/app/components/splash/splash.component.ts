import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss'],
})
export class SplashComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private utilServ: GenralUtilsService,
    public modalController: ModalController) {
    localStorage.removeItem('firstTime');
    this.check();
  }

  ngOnInit() { }
  check() {
    console.log('Yaa Check')
    const init = setInterval(() => {
      if (this.utilServ.friendsList && this.utilServ.sportsList && this.utilServ.countryList && this.utilServ.langSetupFLag) {
        this.close();
        clearInterval(init);
      }
      else {
        this.check();
      }
    }, 500);
  }
  close() {
    // console.log('close is called');
    this.utilServ.navTimeline();
    this.modalController.dismiss();
  }
}
