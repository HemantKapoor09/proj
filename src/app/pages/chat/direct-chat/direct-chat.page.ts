import { Component, OnInit, ViewChild, ElementRef, } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { Socket } from 'ngx-socket-io';
import { IonContent } from '@ionic/angular';
import { IonInfiniteScroll, ModalController } from '@ionic/angular';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { ApiService } from 'src/app/services/api.service';
import * as moment from 'moment';
import { element } from 'protractor';
import { DeviceNativeApiService } from 'src/app/services/device-native-api.service';
import { EventsCustomService } from 'src/app/services/events-custom.service';

@Component({
  selector: 'app-direct-chat',
  templateUrl: './direct-chat.page.html',
  styleUrls: ['./direct-chat.page.scss'],
})
export class DirectChatPage implements OnInit {

  @ViewChild(IonContent) contentArea: IonContent;
  @ViewChild('myStyle') recentTagHtml: any;
  @ViewChild('myStyleRcv') recentRcvTagHtml: any;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild('messageToSendBox') messageBox;
  items: any[] = [];
  socket: any;
  chatInput = '';
  loading: any;
  chatdata: any;
  usersdetail: any;
  chatUserId: any;
  data: any;
  environment: any;
  data1: any;
  fileToUpload: File = null;
  imagearray = [];
  fileToUploadFileName = [];
  chats = [];
  fileToUploadlist = [];
  imageurls = [];
  imagedata1: any;
  language: any;
  offset = 0;
  limit = 100;
  firsttime = true;
  lastname: any;
  senderprofileinfo: any;
  picUrl: any;
  profilename: any = '';
  profileid: any;
  noData = false;
  loadingmsg = false;
  loadingerrormsg: boolean;
  chatss: any[];
  yesterday: any;
  today: any;
  chatArray: any;
  messageGroup: any;
  localDate: string;
  onlineMode = false;
  volumeOff = false;
  volumeOn = true;
  limitReached = false;
  imageUrl = '';
  tempArry = [];
  onDirectChatPage = '1';

  // String
  saySomethingString = 'Say something...';
  noConversationsString = 'No Conversations';
  todayString = 'Today';
  yesterdayString = 'Yesterday';
  daysAgoString = '%X day(s) ago';
  minAgoString = '%X minute(s) ago';
  hoursAgoString = '%X hour(s) ago';
  weekAgoString = '%X week(s) ago';
  monthAgoString = '%X month(s) ago';
  yearAgoString = '%X year(s) ago';
  dayAgoString = '%X day(s) ago';
  minsAgoString = 'a minutes ago';
  hourAgoString = 'An hour ago';
  weeksAgoString = '%X week(s) ago';
  monthsAgoString = '%X month(s) ago';
  yearsAgoString = '%X year(s) ago';
  dateString = 'Date';
  fewSecAgoString = 'a few seconds ago';
  aDayAgoString = '%X day(s) ago';
  anHourAgoString = 'An hour ago';
  anHoursAgoString = '%X hour(s) ago';
  takePhotoString = 'Take photo';
  chooseImage = 'Choose image';
  volumeStatus = 'Volume';
  session: any;
  totalChats: any;

  constructor(
    private socketAPI: Socket,
    private actRouter: ActivatedRoute,
    private utilServ: GenralUtilsService,
    private router: Router,
    private nativeLib: DeviceNativeApiService,
    private eventCustom: EventsCustomService,
    private apiService: ApiService,
    private location: Location) {
    this.actRouter.params.subscribe(data => {
      this.chatUserId = data.id;
      this.usersdetail = JSON.parse(this.utilServ.getUserDetails());
      this.getChatDetails(this.chatUserId);
      this.setMessageReadFlag(this.usersdetail.id, this.chatUserId);
      this.socketAPI.on('receive-message', (msg) => {
        this.apiService.connectFuntion();
        this.getChatDetails(this.chatUserId);
        if (this.onDirectChatPage === '1') {
          this.setMessageReadFlag(this.usersdetail.id, this.chatUserId);
        }
      });
    });
  }

  ngOnInit() {
    this.environment = environment;
  }

  ionViewWillEnter() {
    this.apiService.connectFuntion();
    this.today = new Date().toISOString().slice(0, 10);
    this.yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000).toISOString().slice(0, 10);

    this.apiService.getUserBaseInfoByUserId(this.chatUserId, this.chatUserId).pipe().subscribe((res: any) => {
      this.senderprofileinfo = res.message;
      this.profilename = this.senderprofileinfo.baseInfo.first_name + ' ' + this.senderprofileinfo.baseInfo.last_name;
      this.lastname = this.senderprofileinfo.baseInfo.last_name;
      this.picUrl = this.senderprofileinfo.baseInfo.profile_img_url_thump;
      this.profileid = this.senderprofileinfo.baseInfo.id;
      this.contentArea.scrollToBottom();
    },
      error => {
        console.log(error);
      });
  }
  async presentActionSheet() {
    this.nativeLib.presentActionSheetCamOrGall();
    this.eventCustom.subscribe('imageReady', (imgData) => {

      this.imagearray.push(imgData.webPath);    // Direct setting to the view
      this.fileToUploadlist.push(imgData.blob);
      // tslint:disable-next-line: prefer-for-of
      for (let s = 0; s < this.fileToUploadlist.length; s++) {
        this.apiService.chatimgupload(this.fileToUploadlist[s], this.session.id, imgData.reqPath).pipe().subscribe(data => {
          this.data1 = data;
          this.imageurls.push(this.data1.message);
        },
          (error: any) => {
            console.log('Error in select media section ::', error);
          });
      }
    });
  }

  removeimg(i) {
    if (i > -1) {
      this.imageurls.splice(i, 1);
      this.imagearray.splice(i, 1);
    }
  }
  uniqBy(a, key) { }

  moveFocus(nextElement) {
    nextElement.setFocus();
  }

  volumeControl(value) {
    if (value === '2') {
      this.volumeOff = true;
      this.volumeOn = false;
      localStorage.setItem('volumeControl', '2');
    }
    if (value === '1') {
      this.volumeOn = true;
      this.volumeOff = false;
      localStorage.setItem('volumeControl', '1');
    }
  }

  getChatDetails(otherid) {

    this.apiService.getChatHistory(this.usersdetail.id, otherid, this.offset, this.limit).pipe().subscribe((res: any) => {
      this.totalChats = res.message.meta.total;
      this.checkIfChatsExist();

      if (this.totalChats !== 0) {
        this.apiService.connectFuntion();
        console.log('ChatAry', res);

        this.chats = res.message.content;
        const groups = this.chats.reduce((groups, message) => {
          const date = new Date(message.date).toISOString().slice(0, 10);
          if (!groups[date]) {
            groups[date] = [];
          }
          groups[date].push(message);
          return groups;
        }, {});


        const groupArrays = Object.keys(groups).map((date) => {
          return {
            date,
            message: groups[date]
          };
        });
        this.chatArray = groupArrays;
        let something;
        this.chatArray.flat();
        this.chatArray.forEach(el => {
          something = el.message;
          if (el.date === this.today) {
            el.date = el.date.replace(el.date, this.todayString);
          } else if (el.date === this.yesterday) {
            el.date = el.date.replace(el.date, this.yesterdayString);
          } else {
            el.date = moment(el.date).format('ll');
          }
        });
        const last = something.length - 1;
        something[last].recent = true;
        setTimeout(() => {
          this.moveFocus(this.recentTagHtml);
          this.messageBox.setFocus();
        }, 500);
      } else {
        this.checkIfChatsExist();
      }

    });

  }


  loadMoreMessage(e) {
    // if (this.chats.length) {
    //   if (this.totalChats === this.chats.length) {
    //     this.limitReached = true;
    //   } else {
    //    // this.offset += 100;
    //     this.limit += 100;
    //     this.getChatDetails(this.chatUserId);
    //   }
    // }
  }

  sendMessageFun(mssage) {

    const msg = this.formatString(mssage);

    if (msg !== '' || this.fileToUploadFileName !== ['']) {
      if (msg !== '' && this.data1 === undefined) {
        this.data = {
          partner: this.chatUserId,
          message: {
            userinfo: {
              id: this.usersdetail.id,
              imgurl: this.usersdetail.profile_img_url,
              name: this.usersdetail.first_name
            },
            content: msg,
            media_url: '',
            date: new Date(),
            calltype: 'out',
            type: 'txt'
          }
        };

        this.socketAPI.emit('send-message', this.data);
        this.chatInput = '';
      } else if (this.imageurls !== []) {
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < this.imageurls.length; i++) {
          this.dateString = moment(new Date()).format('LT');
          this.chatdata = {
            partner: this.chatUserId,
            message: {
              content: msg,
              media_url: this.imageurls[i],
              date: new Date(),
              type: 'media',
              userinfo: {
                id: this.usersdetail.id,
                imgurl: this.usersdetail.profile_img_url,
                name: this.usersdetail.first_name
              },
            }
          };

          this.socketAPI.emit('send-image-to-server', this.chatdata);
        }
        this.imagearray = [];
        this.imageurls = [];
        this.fileToUploadlist = [];
        this.chatInput = '';
      }
    }
    // this.getChatDetails(this.chatUserId);
  }

  handleImgError(ev: any) {
    const source = ev.srcElement;
    const imgSrc = 'https://storage.googleapis.com/tribation_uploads_dev/noimage.png';
    source.src = imgSrc;
  }

  back() {
    this.onDirectChatPage = '0';
    this.utilServ.navMainChat();
  }

  // ---------------------IMAGE VIEW------------------------------------//
  async viewImage(src: string, title: string, description: string = '') {
    // let image = `${environment.apiUrl}v1/api/post/imagecall_mobile_url?imagepath=${src}`;
    // const modal = await this.modalController.create({
    //   component: ImageViewComponent,
    //   componentProps: {
    //     imgSource: image,
    //     imgTitle: title,
    //     imgDescription: description
    //   },
    //   cssClass: 'modal-fullscreen',
    //   keyboardClose: true,
    //   showBackdrop: true
    // });

    // return await modal.present();
  }
  friendDetail(id) {
    this.router.navigate([`profile/${id}`]);
  }
  formatString(strng) {
    return this.utilServ.formatString(strng);
  }
  openKeyboard() {
    this.utilServ.showKeyBoard();
  }
  setMessageReadFlag(myUserId, chatUserId) {
    console.log(this.usersdetail.id, this.chatUserId);
    this.apiService.setReadMessageFlag(myUserId, chatUserId).pipe().subscribe((res: any) => {
      console.log('res', res);
    });
  }

  checkIfChatsExist() {
    if (this.totalChats !== 0) {
      this.noData = false;
    } else {
      console.log('dfhg');
      this.noData = true;
    }
  }

}
