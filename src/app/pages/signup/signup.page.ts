import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder, ValidatorFn, AbstractControl } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { DeviceNativeApiService } from 'src/app/services/device-native-api.service';
import { NavController, Platform, ActionSheetController, LoadingController, AlertController, ModalController } from '@ionic/angular';
import { EventsCustomService } from 'src/app/services/events-custom.service';
import { SelectCountryComponent } from 'src/app/components/select-country/select-country.component';

const trimValidator: ValidatorFn = (control: FormControl) => {
  if (control.value) {
    if (control.value.startsWith(' ')) {
      return {
        'trimError': { value: 'control has leading whitespace' }
      };
    }
  }
  return null;
};

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})

export class SignupPage implements OnInit {
  signup: FormGroup;
  maxDate: string;
  minDate: string;
  signupvalidation: any;
  countryCode;
  invalidEmail: boolean;
  recaptcha_value: boolean = false;
  email;
  recaptchaValue = false;
  invalid_email: boolean;
  countryName: string;
  countryValue = '0';
  environment: any;
  pswrd: any;
  pic_url: any = '';
  exitEmailString: any;
  Value: any;
  blob: any;
  picToSend: any = '';

  lastnameModel;
  firstnameModel;
  emailModel;
  passwordModel;
  newPasswordModel;
  dobModel;
  genderModel;
  cityModel;
  countryModel;
  termsModel;
  showcPasswordText;
  localLanguage: string;
  profileImage: any;
  blobImg: any = null;

  // Init
  hide = true;
  passwordType = 'password';
  passwordIcon = 'eye-off';

  passwordConType = 'password';
  passwordConIcon = 'eye-off';


  // Strings

  createAccountString = 'Create Account';
  accountCreatedString = 'Account Created';
  lastNameString = 'Last Name';
  firstNameString = 'First Name';
  emailString = 'Email';
  passwordString = 'Password';
  newPasswordString = 'Confirm password';
  DOBString = 'Date of Birth';
  maleString = 'Male';
  femaleString = 'Female';
  citYString = 'City';
  selectCountryString = 'Select Country';
  registerNowString = 'Register Now';
  termsIAcceptString = 'I accept the';
  termsConditionString = 'Terms and Conditions';
  termsUsingFeatureString = 'of using this feature';
  signInString = 'Login';


  alreadyhaveaccountString = 'I already have an account';
  noSpaceString = 'No Space';
  enterLastNameString = 'Please Enter your Last Name';
  enterFirstNameString = 'Please First Name';
  emailErrorString = 'Email Syntax Error';
  enterEmailString = 'Please Enter your Email';
  enterPasswordString = 'Please Enter your password';
  password6charString = 'Paasowrd should be more then 6 char';
  password30charString = 'password should be 30 char short';
  PatternDoesNotMatchString = 'Pattern Does not match';
  confirmPasswordRequiredString = 'Please Confirm Password';
  passwordNotMatchString = 'Password does not match';
  enterDobString = 'Please Enter data of birth';
  selectGenderString = 'Please select Gender';
  enterCitySting = 'please Enter City';
  pleaseSelectCountrySting = 'Please Select Country';
  setImage: any;


  constructor(
    private apiService: ApiService,
    private utilServ: GenralUtilsService,
    private actRouter: ActivatedRoute,
    private eventCustom: EventsCustomService,
    private nativeLib: DeviceNativeApiService,
    private modalController: ModalController) {
    this.actRouter.queryParams.subscribe(() => {

      this.maxDate = this.utilServ.formatDateMin();
      this.minDate = this.utilServ.formatDateMax();

      this.createform1();

      this.apiService.signup_validation().pipe().subscribe((res: any) => {
        console.log(res);
        this.signupvalidation = res.message.info;
      });

    });
  }

  ngOnInit() {
    this.environment = environment;
    this.localLanguage = localStorage.getItem('deviceLanguage') || 'english';
  }

  presentActionSheet() {
    this.nativeLib.presentActionSheetCamOrGall();

    this.eventCustom.subscribe('imageReady', (imgData) => {
      this.setImage = imgData.webPath;
      this.apiService.signup_mobile_temp(imgData.blob).pipe().subscribe((data: any) => {
        if (data.success === 1) {
          this.picToSend = String(data.message);
        } else { console.log("could't get image data"); }
      },
        (error: any) => {
          alert("Error" + JSON.stringify(error));
        });
    });
  }

  private createform1() {
    this.signup = new FormGroup({
      dob: new FormControl('', []),
      firstname: new FormControl('', Validators.compose([Validators.required, trimValidator])),
      lastname: new FormControl('', Validators.compose([Validators.required, trimValidator])),
      email: new FormControl('', Validators.compose([Validators.required, Validators.email, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])),
      gender: new FormControl('', []),
      city: new FormControl('', Validators.compose([Validators.required, trimValidator])),
      country: new FormControl('', Validators.required),
      terms: new FormControl(false, Validators.pattern('true')),
      password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(30), trimValidator, Validators.pattern(/(?=^.{6,30}$)((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*/)])),
      confirmpassword: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(30), trimValidator, this.equalto('password'), Validators.pattern(/(?=^.{6,30}$)((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*/)]))
    });
  }

  check() {
    this.signup.controls['confirmpassword'].reset();
  }

  signUpFun() {
    this.apiService.signup_mobile(
      this.signup.value.firstname,
      this.signup.value.lastname,
      this.signup.value.email,
      this.signup.value.confirmpassword,
      this.signup.value.dob,
      this.signup.value.gender,
      this.signup.value.city,
      this.countryValue,
      this.localLanguage,
      this.picToSend).pipe().subscribe((res: any) => {
        console.log(res);
        if (res.success === 0) {
          this.exitEmail();
        } else if (res.success === 1) {
          this.signup.reset();
          this.successSignup();
        }
      }),
      error => {
        this.utilServ.okButtonMessageAlert('403 Forbidden / Access denied!\nAccess denied \n error');
      };
    localStorage.removeItem('signUp');
  }

  resolved(captchaResponse: string) {
    if (captchaResponse) {
      this.recaptcha_value = true;
    }
  }

  // password validation
  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): {
      [key: string]: any
    } => {
      let input = control.value;
      let isValid = control.root.value[field_name] === input;
      if (!isValid)
        return {
          'equalTo': {
            isValid
          }
        };
      else
        return null;
    };
  }


  email_field() {
    this.invalid_email = false;
  }

  emailTrim() {
    this.email = this.signup.value.email.trim();
    let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(this.email) === false) {
      this.invalid_email = true;
      return false;
    }
    return true;
  }

  // Choose Country
  async gotocountry() {
    const modal = await this.modalController.create({
      component: SelectCountryComponent,
    });
    modal.onDidDismiss().then(data => {
      if (data.data.data) {
        this.countryModel = data.data.data.countryName;
        this.countryValue = data.data.data.countryCode;
        this.signup.get('country').setValue(`${this.countryModel}`)
      }
    });
    return await modal.present();
  }

  successSignup() {
    this.utilServ.okButtonMessageAlert(this.accountCreatedString);
  }

  exitEmail() {
    this.utilServ.okButtonMessageAlert(this.exitEmailString);
  }

  backtologin() {
    this.utilServ.navLogin();
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
  hideShowConPassword() {
    this.passwordConType = this.passwordConType === 'text' ? 'password' : 'text';
    this.passwordConIcon = this.passwordConIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
  moveFocus(nextElement) {
    nextElement.setFocus();
  }
}

