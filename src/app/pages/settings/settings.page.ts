import { Component, OnInit } from '@angular/core';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  userDetail;

  // Strings
  settingsString = 'Sengs';
  accountString = 'Account';
  aboutString = 'About';
  editProfileString = 'Edit Profile';
  privacySecurityString = 'Privacy Security';
  changePasswordString = 'Change Password';
  languageString = 'Language';
  notificationsString = 'Notification';
  helpCenterString = 'Help Center';
  reportProblemString = 'Report a Problem';
  constructor(
    private actRouter: ActivatedRoute,
    private utilServ: GenralUtilsService) {
    this.actRouter.queryParams.subscribe(() => {
      this.userDetail = JSON.parse(this.utilServ.getUserDetails());
      if (this.utilServ.langSetupFLag) {
        this.localLang();
      }
    });
  }

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.utilServ.checkUserExists();
    this.actRouter.queryParams.subscribe(() => {
      this.userDetail = JSON.parse(this.utilServ.getUserDetails());
      if (this.utilServ.langSetupFLag) {
        this.localLang();
      }
    });
  }
  localLang() {
    this.settingsString = this.utilServ.getLangByCode('settings');
    this.accountString = this.utilServ.getLangByCode('account');
    this.aboutString = this.utilServ.getLangByCode('about');
    this.editProfileString = this.utilServ.getLangByCode('edit_profile');
    this.privacySecurityString = this.utilServ.getLangByCode('privacy_security');
    this.changePasswordString = this.utilServ.getLangByCode('change_password');
    this.languageString = this.utilServ.getLangByCode('language');
    this.notificationsString = this.utilServ.getLangByCode('notifications');
    this.helpCenterString = this.utilServ.getLangByCode('help_center');
    this.reportProblemString = this.utilServ.getLangByCode('report_problem');
  }
  scoutCantEdit() {
    this.utilServ.presentToast('As you are scout please contact Tibation for editing your name');
  }
}
