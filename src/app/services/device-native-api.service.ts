import { Injectable } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { ActionSheetController, Platform, AlertController } from '@ionic/angular';
import {
  Plugins, CameraResultType, Capacitor, FilesystemDirectory,
  CameraPhoto, CameraSource, PromptResult
} from '@capacitor/core';
import { EventsCustomService } from './events-custom.service';
import { ApiService } from './api.service';
import { GenralUtilsService } from './genral-utils.service';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { File, DirectoryEntry } from '@ionic-native/file/ngx';
const { Camera, Filesystem, Storage } = Plugins;

interface Photo {
  filepath: string;
  webviewPath: string;
  base64?: string;
}

@Injectable({
  providedIn: 'root'
})
export class DeviceNativeApiService {

  // public photos: Photo[] = [];
  public blobURL: any; // Image
  public blob: any;    // Blob

  albumsString = 'Albums';
  cameraString = 'Camera';
  galleryString = 'Gallery';

  imageURL;
  userDetail;
  tempTest: any;
  sanitizer: any;


  constructor(
    private actionSheetController: ActionSheetController,
    private platform: Platform,
    private apiService: ApiService,
    private utilServ: GenralUtilsService,
    private eventCustom: EventsCustomService,
    private androidPermissions: AndroidPermissions,
    public alertController: AlertController,
    private mediaCapture: MediaCapture,
    private file: File) {
    this.getPermissions();
    this.platform = platform;
    this.userDetail = JSON.parse(this.utilServ.getUserDetails());
  }
  getPermissions() {
    this.androidPermissions.requestPermissions(
      [
        this.androidPermissions.PERMISSION.CAMERA,
        this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION,
        this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE,
        this.androidPermissions.PERMISSION.READ_PHONE_STATE,
        this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE
      ]
    );
  }
  // Take a photo
  async takePicture(CameraSource) {
    this.apiService.connectFuntion();
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource,
      quality: 30,
      width: 600,
      height: 600
    });
    const x = { webPath: capturedPhoto.webPath, blob: null, reqPath: null };
    x.reqPath = capturedPhoto.path;
    const blb = await fetch(capturedPhoto.webPath).then(r => r.blob());
    x.blob = blb;
    this.eventCustom.publish('imageReady', x);
    return capturedPhoto.webPath;
  }

  async presentActionSheetCamOrGall() {
    const actionSheet = await this.actionSheetController.create({
      header: this.albumsString,
      buttons: [{
        text: this.cameraString,
        role: 'destructive',
        icon: 'camera',
        handler: () => {
          this.takePicture(CameraSource.Camera);
        }
      }, {
        text: this.galleryString,
        icon: 'image',
        handler: () => {
          this.takePicture(CameraSource.Photos);
        }
      }]
    });
    await actionSheet.present();
  }

  // Timeline - upload post
  async captureImage(CameraSource) {
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource,
      quality: 20,
      width: 600,
      height: 600
    });
    let x = { mediaUrl: capturedPhoto.webPath, blob: null, filename: null, type: null };
    const newFileName = this.formateFileName(capturedPhoto.path);
    x.filename = newFileName;
    const blb = await fetch(capturedPhoto.webPath).then(r => r.blob());
    x.blob = blb;
    x.type = blb.type;
    this.eventCustom.publish('imageReady', x);
  }
  async captureVideo() {
    const options: CaptureVideoOptions = {
      limit: 1,
      duration: 30,
      quality: 0
    };
    let capturedVideo = await this.mediaCapture.captureVideo(options);
    const webPathVideo = Capacitor.convertFileSrc(capturedVideo[0].fullPath);
    const response = await fetch(webPathVideo);
    const blb = await response.blob();
    const newFileName = this.formateFileName(capturedVideo[0].name);
    const x = {
      mediaUrl: webPathVideo,
      blob: blb,
      filename: newFileName,
      type: capturedVideo[0].type
    };
    console.log(x);
    this.eventCustom.publish('videoReady', x);
  }

  formateFileName(filename) {
    let nameBeforeSpace: string = (filename.split(' '))[0];
    nameBeforeSpace = nameBeforeSpace.replace(/\s/g, '_');
    nameBeforeSpace = nameBeforeSpace.replace(/\./g, '_');
    const ext = (filename.split('.')).pop();
    const newFileName = this.userDetail.id + '_' + (this.utilServ.getTimeStamp()) + '_' + nameBeforeSpace + '.' + ext;
    return newFileName;
  }
}

