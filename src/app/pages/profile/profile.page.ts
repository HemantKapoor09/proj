import { Component, OnInit, ElementRef } from '@angular/core';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, ToastController, AlertController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { ProfilePortfolioComponent } from 'src/app/components/profile-portfolio/profile-portfolio.component';
import { environment } from 'src/environments/environment';
import { Location } from '@angular/common';
import * as moment from 'moment';
import { MultimediaViewComponent } from 'src/app/components/multimedia-view/multimedia-view.component';
import { CommentOnPostComponent } from 'src/app/components/comment-on-post/comment-on-post.component';
import { Socket } from 'ngx-socket-io';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  environment;
  profile: any;
  profileId;
  coverUrl = '../../../assets/tribation.jpg';
  currentLocation: string;
  userProfileDetail: any = null;
  totoalPost: any;
  postsArray: any[] = [];
  userDetail;
  checkedSegment;
  personalMedia: any[] = [];
  postString = 'Post';
  sharedString = 'Shared';
  portfolioString = 'Portfolio';
  mediaString = 'Media';
  homeString = 'Home';
  friendsString = 'Friends';
  someonesPostString = '\'s post';
  unfriendString = 'Unfriend';
  friendRequestSentString = 'Friend request sent';
  unfriendConfirmString = 'Are you sure you want to unfriend ';
  cancelReqConfirmString = 'Are you sure you want to cancel friend request';
  cancelString = 'Cancel';
  okayString = 'OK';
  totalPersonalMedia: any;
  friendsList: any[];
  myfrndsList: any[];
  scoutVisting = false;
  limitReached = false;
  constructor(
    private location: Location,
    private actRouter: ActivatedRoute,
    public alertController: AlertController,
    public toastController: ToastController,
    public socket: Socket,
    private router: Router,
    private modalcontrolller: ModalController,
    private element: ElementRef,
    private modalController: ModalController,
    private apiService: ApiService,
    private utilServ: GenralUtilsService) {
    this.actRouter.queryParams.subscribe(() => {
      this.userDetail = JSON.parse(this.utilServ.getUserDetails());
      console.log(this.userDetail);
    });
  }

  ngOnInit() {
    this.environment = environment;
    // this.profile = {
    //   id: 4,
    //   pic_url: '',
    //   first_name: 'Hemant',
    //   last_name: 'Kapoor'
    // };
  }

  ionViewWillEnter() {
    this.checkedSegment = 'home';
    this.utilServ.checkUserExists();
    this.currentLocation = JSON.parse(localStorage.getItem('currentLocation'));
    const x: string = this.currentLocation.substr(9, this.currentLocation.length);
    this.profileId = Number(x.split('/')[0]);
    if (x.split('/')[1] === 'portfolio') {
      this.scoutVisting = true;
      this.openProtfolio();
    }
    this.loadPost();
    console.log(this.profileId);
  }


  loadMorePost(eve) {
    if (this.postsArray.length === this.totoalPost) {
      this.limitReached = true;
    } else {
      this.loadPost();
    }
    setTimeout(() => {
      eve.target.complete();
    }, 1000);
  }

  loadPost() {
    let offset = 0;
    if (this.postsArray.length !== 0) {
      offset = this.postsArray.length;
    }
    if (offset === this.totoalPost) {
      this.limitReached = true;
    }
    this.apiService.getUserProfilePageData(15, offset, Number(this.profileId), this.userDetail.id).subscribe((res: any) => {
      console.log(res);
      if (!this.userProfileDetail) {
        this.userProfileDetail = res.message.user;
        console.log(this.userProfileDetail);
        if (this.userProfileDetail.profile_bg_img_url !== '') {
          this.coverUrl = `${environment.apiUrl}v1/api/post/imagecall_mobile_url?imagepath=profile/${this.profileId}/${this.userProfileDetail.profile_bg_img_url}`;
        }
        console.log('coverUrl', this.coverUrl);
      }
      if (!this.totoalPost) { this.totoalPost = res.message.meta.total; }
      if (this.postsArray.length === 0) {
        this.postsArray = res.message.content;
      } else {
        // console.log(this.postsArray.length, " <= len :: Total=> ", this.totoalPost, '\nOffset Sent ', offset);
        if (this.postsArray.length === this.totoalPost) {
          this.limitReached = true;
        } else {
          res.message.content.forEach(post => {
            this.postsArray.push(post);
          });
        }
      }
      this.dontReloadMension();

      // setTimeout(() => {
      // console.log(this.postsArray);
      // }, 12000);
    });
  }
  likePost(postId, xx) {
    if (this.postsArray[xx].liked === 1) {
      this.postsArray[xx].liked = 0;
      this.postsArray[xx].likes -= 1;
      this.apiService.unLikePost(this.userDetail.id, postId).subscribe((lyk: any) => {
        console.log(lyk);
      });
    } else {
      this.postsArray[xx].liked = 1;
      this.postsArray[xx].likes += 1;
      this.apiService.likePost(this.userDetail.id, postId).subscribe((lyk: any) => {
        console.log(lyk);
      });
    }
  }

  sharePost(postId, xx) {
    console.log(xx);
    this.apiService.sharePost(this.userDetail.id, postId).subscribe((shre: any) => {
      console.log(shre);
    });
  }
  async commentPost(feed, xx) {
    const feedDataComment = {
      postId: feed.id,
      userId: feed.user.id
    };
    localStorage.setItem('commentOnPost', JSON.stringify(feedDataComment));
    const modal = await this.modalController.create({
      component: CommentOnPostComponent,
      cssClass: 'comment_of_post',
    });
    modal.onDidDismiss().then((commentRes) => {
      localStorage.removeItem('commentOnPost');
      console.table(commentRes);
    });
    return await modal.present();
  }

  private dontReloadMension(): void {
    setTimeout(() => {
      const urls: any = this.element.nativeElement.querySelectorAll('a');
      urls.forEach((url) => {
        url.addEventListener('click', (event) => {
          event.preventDefault();
          const textOfLink = event.target.innerText;
          console.log(textOfLink);
          const x = event.target.href.split(/[\s/]+/);
          this.router.navigate([`profile/${x[x.length - 1]}`]);
        }, false);

      });
    }, 1500);
  }
  setupBasic() { }
  async openProtfolio() {
    if (this.scoutVisting === true) {
      localStorage.setItem('portfolioByScout', 'true');
    }
    const modal = await this.modalController.create({
      component: ProfilePortfolioComponent
    });
    modal.onDidDismiss().then((protfolioRes) => {
      localStorage.removeItem('portfolioByScout');
      if (this.scoutVisting === true) {
        this.router.navigate(['/scouts']);
      }
      this.checkedSegment = 'home';
      console.table(protfolioRes);
    });

    return await modal.present();
  }
  back() {
    this.location.back();
  }
  getTimeFromNow(createdDate) {
    return moment(createdDate).fromNow();
  }
  segmentChanged(e) {
    if (this.checkedSegment === 'media') {
      this.getAsserts();
    }
    if (this.checkedSegment === 'friends') {
      this.getFriends();
    }
  }
  getAsserts() {
    let nullVall;
    this.apiService.getAssets(this.profileId, 0, 100, nullVall).subscribe((res: any) => {
      this.personalMedia = res.message.content;
      this.totalPersonalMedia = res.message.meta.total;
    });
  }
  async openMultiMedia(fullAsserts, i) {
    const xDx = {
      asserts: fullAsserts,
      currentIndex: i
    };
    localStorage.setItem('mediaArray', JSON.stringify(xDx));
    const modal = await this.modalcontrolller.create({
      component: MultimediaViewComponent,
      // cssClass: 'sharePOP',
      componentProps: {
        // custom_id: this.shareValue
        swipeToClose: true
      }
    });
    return await modal.present();
  }
  getFriends() {
    this.apiService.getFriendsList(this.profileId).subscribe((friends: any) => {
      this.myfrndsList = friends.message;
      this.friendsList = this.myfrndsList;
      console.log(this.friendsList);
    });
  }
  viewProfile(x) {
    this.router.navigate([`/profile/${x}`]);
  }
  async cancelFriendreq(userId) {
    const alert = await this.alertController.create({
      header: this.cancelString,
      message: this.cancelReqConfirmString,
      buttons: [
        {
          text: this.cancelString,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: this.okayString,
          handler: () => {
            const data = {
              cf_id: userId,
              hf_id: this.userDetail.id
            };
            this.socket.emit('cancel-friend-request', data);
          }
        }
      ]
    });
    await alert.present();
  }
  sendFriendReq(UserId) {
    const data = {
      cf_id: this.userDetail.id,
      hf_id: UserId
    };

    this.socket.emit('send-friend-request', data);
  }
  async unFriend(userId, frndName) {
    const confirm = await this.alertController.create({
      header: this.unfriendString + '?',
      // message: this.unfriendConfirmString.replace('%X', frndName),
      message: this.unfriendConfirmString + frndName + '?',
      buttons: [
        {
          text: this.cancelString,
          role: 'cancel'
        },
        {
          text: this.okayString,
          handler: () => {
            const data = {
              cf_id: userId,
              hf_id: this.userDetail.id
            };
            this.socket.connect();
            this.socket.emit('cancel-friend-request', data);
            // this.myfrndsList = '';
            this.utilServ.friendsList = null;
            this.utilServ.userAddonDetails();
            this.myfrndsList = this.utilServ.friendsList;
          }
        }
      ]
    });
    confirm.present();
  }
  async frndreqToast() {
    const toast = await this.toastController.create({
      message: this.friendRequestSentString,
      duration: 2000,
      position: 'top',
    });
    toast.present();
  }

}
