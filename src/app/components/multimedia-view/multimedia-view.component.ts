import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonSlides, ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-multimedia-view',
  templateUrl: './multimedia-view.component.html',
  styleUrls: ['./multimedia-view.component.scss'],
})
export class MultimediaViewComponent implements OnInit {
  environment;

  @ViewChild('slider', { read: ElementRef }) slider: ElementRef;
  @ViewChild(IonSlides) slides: IonSlides;
  @ViewChild('myVideo') myVideo: ElementRef;
  imageArray: any = '';
  index: any;
  sliderOpts: any;
  currentIndex: number;
  dataRes: any;

  constructor(
    private modalController: ModalController,
    private actRouter: ActivatedRoute,) {
    this.actRouter.queryParams.subscribe(params => {
      this.setupBasic();
    });
  }

  ngOnInit() {
    this.environment = environment;
  }
  setupBasic() {
    this.dataRes = JSON.parse(localStorage.getItem('mediaArray'));
    this.imageArray = this.dataRes.asserts;
    this.index = this.dataRes.currentIndex;
    console.log(this.imageArray);
    this.sliderOpts = {
      centeredSlides: true,
      slidesPerView: 1,
      zoom: true,
      speed: 10,
      setWrapperSize: true,
      initialSlide: this.dataRes.currentIndex,
      watchOverflow: true,
      centeredSlidesBounds: true,
      rotate: 50,
      stretch: 0,
      depth: 500,
      modifier: 1,
      // slideShadows: true,
    };
    setTimeout(() => {
      this.slides.slideTo(this.index, 10);

    }, 300);
    
  }
  close() {
    localStorage.removeItem('mediaArray');
    this.imageArray = null;
    this.modalController.dismiss();
  }
  zoom(zoomIn: boolean) {
    let zoom = this.slider.nativeElement.swiper.zoom;
    if (zoomIn) {
      zoom.in();
    } else {
      zoom.out();
    }
  }

  slideChanged(event) {
    this.index = this.slides.getActiveIndex();
    this.slides.getActiveIndex().then(index => {
      var currentIndex = index;
      var nextindex = index + 1;
      if (this.imageArray[nextindex]) {
        if (this.imageArray[nextindex].post_type !== 'image') {
          var vid1 = <HTMLVideoElement>document.getElementById('myVideo' + nextindex);
          vid1.pause();
        }
      }

    });
    this.slides.getPreviousIndex().then(index => {
      var previousIndex = index
      // console.log(index);
      // console.log('currentIndex:', index);
      if (this.imageArray[previousIndex]) {
        if (this.imageArray[previousIndex].post_type != 'image') {
          var vid1 = <HTMLVideoElement>document.getElementById('myVideo' + previousIndex);
          vid1.pause();
        }
      }
    });

    // this.slides.slideNext(){}

  }
  swipeUp(e): any {
    this.modalController.dismiss();
  }
}
