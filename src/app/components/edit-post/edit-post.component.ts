import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { DeviceNativeApiService } from 'src/app/services/device-native-api.service';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { CameraSource } from '@capacitor/core';
import { EventsCustomService } from 'src/app/services/events-custom.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss'],
})
export class EditPostComponent implements OnInit {
  @ViewChild('postText') postText: any;
  @ViewChild('userInput') userInputViewChild: ElementRef;

  environment;
  userDetail;
  postData;
  legacySportIcon = '';
  postUser;
  postContent;
  originalPostContent;
  showMensionPickUp = false;
  friendsList: any;
  atCount: any;
  currentSerachTag;
  currentSearchArray: string[];
  searchFriendsList: any;
  tempFriendTag: any;
  tempReplacement: any[] = [];
  tempPostContent: string;
  userInputElement: HTMLInputElement;
  ifEditied = false;
  mediaArray = [];
  progressBarval = '0';
  imageExtArray = ['image/gif', 'image/jpeg', 'image/png', 'image/raw', 'image/svg', 'image/heic'];
  vidExtArray = ['video/webm', 'video/mpg', 'video/ogg', 'video/mp4', 'video/avi', 'video/mov'];

  // String
  editPostString = 'Edit Post';
  saveString = 'Save';
  constructor(
    private apiService: ApiService,
    private router: Router,
    private element: ElementRef,
    private nativeLib: DeviceNativeApiService,
    private utilServ: GenralUtilsService,
    private actionSheetController: ActionSheetController,
    private modalController: ModalController,
    private sanitizer: DomSanitizer,
    private eventCustom: EventsCustomService,) {
    this.userDetail = JSON.parse(this.utilServ.getUserDetails());
    this.friendsList = this.utilServ.friendsList;
    this.searchFriendsList = this.friendsList;
    this.postData = JSON.parse(localStorage.getItem('postDataEdit'));
    this.postUser = this.postData.user;
    // const x = setInterval(() => {
    //   if (this.xDx.length > 0) {
    //     this.ifEditied = true;
    //     clearInterval(x);
    //   }
    // }, 200);
  }

  ngOnInit() {
    this.environment = environment;
    // console.log(this.postData);
    this.postContent = this.postData.content;
    this.originalPostContent = this.postContent;
    this.mediaArray = this.postData.assets;

    this.makeItViewable();
    if (this.utilServ.langSetupFLag) {
      this.editPostString = this.utilServ.getLangByCode('edit_post');
      this.saveString = this.utilServ.getLangByCode('save');
    }

  }
  ngAfterViewInit() {
    this.userInputElement = this.userInputViewChild.nativeElement;
  }
  makeItViewable() {
    const tempPostContent: string = this.postContent;
    const aTimes = this.occurrences(tempPostContent, '<a href=', false);
    const aCloseTimes = this.occurrences(tempPostContent, '</a>', false);
    const xOpen: string[] = tempPostContent.split(' <a href');
    let poople: any[] = [];
    let poopleName: any[] = [];
    let zOpen = '';
    let zClose = '';
    for (let i = 0; i <= aTimes; i++) {
      zOpen += xOpen[i].replace('class=\'make_a_tag\'> ', '');
    }

    const totalProple = this.occurrences(zOpen, '=\'', false);
    let finalString = '';
    let startWith = '';
    let endWith = '';
    const idContent: string[] = zOpen.split(' =');
    // tslint:disable-next-line: prefer-const
    let temp: any[] = [];
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < idContent.length; i++) {
      temp.push(idContent[i].split('\''));
    }
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < temp.length; i++) {
      const zx: string[] = temp[i];
      if (zx[0]) {
        finalString += zx[0];
      }
      if (zx[1]) {
        poople.push(zx[1]);
        // console.log(JSON.stringify(zx[2]), zx[2].indexOf('</a>'));
        startWith = zx[2].substr(1, (zx[2].indexOf('</a>') - 2));
        poopleName.push(startWith);
        let tempPush = {
          id: zx[1],
          name: startWith,
          replaced: ''
        };
        const till = this.occurrences(startWith, ' ', false);
        for (let xdf = 0; xdf < till; xdf++) {
          startWith = startWith.replace(' ', '_');
        }
        tempPush.replaced = startWith.toUpperCase();
        this.tempReplacement.push(tempPush);
        startWith = '@' + startWith.toUpperCase();
        endWith = zx[2].substr((zx[2].indexOf('</a>') + 4), zx[2].length);
        finalString += ' ' + startWith + endWith;
      }
    }
    if (finalString.indexOf('=') === 0) {
      finalString = finalString.substring(1, finalString.length);
    }
    this.postContent = finalString;

    setTimeout(() => {
      this.moveFocus(this.postText);
    }, 800);
  }

  postContentEdit(x: string) {
    if (this.postContent !== this.originalPostContent) {
      this.ifEditied = true;
    } else {
      this.ifEditied = false;
    }
    if (this.showMensionPickUp === false) {
      this.atCount = x.length;
    }
    this.currentSearchArray = x.split('@');
    this.currentSerachTag = this.currentSearchArray[this.currentSearchArray.length - 1];
    if (this.currentSearchArray.length > 1 && this.showMensionPickUp === true) {
      console.log('curentsearch', this.currentSerachTag.length, ' : ', this.currentSerachTag);
    }

    if (this.showMensionPickUp === true) {
      this.search(this.currentSerachTag);
    }

    this.postContent = this.utilServ.formatString(this.postContent);
  }

  onFriendTag(x) {
    this.tempFriendTag = { id: x.id, name: x.name };
    const t = this.occurrences(this.tempFriendTag.name, ' ', false);
    // tslint:disable-next-line: prefer-for-of
    for (let index = 0; index < t; index++) {
      this.tempFriendTag.name = this.tempFriendTag.name.replace(' ', '_');
      // const element = array[index];
    }
    this.tempFriendTag.name = this.tempFriendTag.name.toUpperCase();
    const ttmp = {
      replaced: this.tempFriendTag.name,
      name: x.name,
      id: this.tempFriendTag.id,
    };
    this.postContent = this.postContent.substr(0, this.atCount) + '' + this.tempFriendTag.name + ' ';
    this.tempReplacement.push(ttmp);
    this.showMensionPickUp = false;
    this.moveFocus(this.postText);
    this.searchFriendsList = this.friendsList;
  }
  search(x) {
    this.searchFriendsList = this.filter(this.friendsList, x || null);
  }
  filter(items: any[], terms: string): any[] {
    if (!items) { return []; }
    if (!terms) { return items; }
    terms = terms.toLowerCase();
    return items.filter(it => {
      return it.name.toLowerCase().includes(terms);
    });
  }
  occurrences(stng, subString, allowOverlapping) {
    stng += '';
    subString += '';
    if (subString.length <= 0) { return (stng.length + 1); }
    let n = 0;
    let pos = 0;
    const step = allowOverlapping ? 1 : subString.length;
    while (true) {
      pos = stng.indexOf(subString, pos);
      if (pos >= 0) {
        ++n;
        pos += step;
      } else { break; }
    }
    return n;
  }
  private dontReloadMension(): void {
    setTimeout(() => {
      const urls: any = this.element.nativeElement.querySelectorAll('a');
      urls.forEach((url) => {
        url.addEventListener('click', (event) => {
          event.preventDefault();
          const textOfLink = event.target.innerText;
          console.log(textOfLink);
          const x = event.target.href.split(/[\s/]+/);
          this.router.navigate([`profile/${x[x.length - 1]}`]);
        }, false);

      });
    }, 1500);
  }
  mensioning(x) {
    if (x === 50) {
      this.showMensionPickUp = true;
    }
  }
  break(x) {
    this.showMensionPickUp = false;
    this.searchFriendsList = this.friendsList;
    this.moveFocus(this.postText);
  }
  moveFocus(nextElement) {
    nextElement.setFocus();
  }

  creatPost() {
    let emptyVar;
    let count = 0;
    let mediaToSend;
    let mensionIds: number[] = [];
    let xDx: any[] = [];
    this.progressBarval = this.progressBarval + 1;

    this.tempPostContent = this.postContent;
    console.log('this.tempReplacement', this.tempReplacement);
    if (this.tempReplacement.length !== 0) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < this.tempReplacement.length; i++) {
        const xDx = this.tempReplacement[i];
        mensionIds.push(xDx.id);
        this.tempPostContent = this.tempPostContent.replace(`@${xDx.replaced}`, ` <a href=\'${xDx.id}\' class=\'make_a_tag\'> ${xDx.name} </a> `);
      }
      if (mensionIds.length === 0) {
        mensionIds = emptyVar;
      }
      const garbageSubString = this.occurrences(this.tempPostContent, '↵', false);
      for (let j = 0; j <= garbageSubString; j++) {
        this.tempPostContent = this.tempPostContent.replace('↵', ' ');
      }
    }
    let mediaToupld: any[] = [];
    let flag = false;
    this.mediaArray.forEach(element => {
      if (!element.asset_url) {
        mediaToupld.push(element);
      }
    });
    if (mediaToupld) {
      mediaToupld.forEach(media => {
        if (media.blob) {
          this.apiService.uploadTimelineMedia(media.blob, this.userDetail.id, media.filename).subscribe((uploadRes: any) => {
            if (uploadRes.success === 1) {
              const temp = {
                originalImage: uploadRes.message.info.name,
                thumbnailImage: uploadRes.message.info.thumbname
              };
              xDx.push(temp);
              count += 1;
              console.log("I am count", count);
              if (count === mediaToupld.length) {
                flag = true;
                this.ifEditied = true;
              }
            }
          });
        } else if (media.id) {
          xDx.push(media);
          count += 1;
          console.log("I am count", count);
          if (count === mediaToupld.length) {
            flag = true;
            this.ifEditied = true;
          }
        }
      });
    }
    if (mediaToupld.length === 0) {
      this.back();
    }
    const uploadInt = setInterval(() => {
      if (flag === true || this.ifEditied) {
        console.log('im if od upload');
        mediaToSend = xDx;
        this.apiService.savePostTimeline(this.postData.id,
          this.userDetail.id, this.tempPostContent, emptyVar, mediaToSend, mensionIds).subscribe((post: any) => {
            console.log(post);
            if (post.success === 1) {
              this.tempPostContent = '';
              this.postContent = '';
              this.mediaArray = [];
              this.progressBarval = '0';
              this.back();
            }
          });
        clearInterval(uploadInt);
      }
    }, 100);
  }
  // ------Branch Riddhi -----

  async attachMediaInPost() {
    const actionSheet = await this.actionSheetController.create({
      header: 'album',

      buttons: [{
        text: 'Take Picture',
        role: 'destructive',
        handler: () => {
          // this.utilServ.showLoaderWait();
          this.captureImage();
        }
      }, {
        text: 'Take Video',
        handler: () => {
          // this.utilServ.showLoaderWait();
          this.captureVideo();
        }
      },
      {
        text: 'Choose Image/Video',
        handler: () => {
          // this.utilServ.showLoaderWait();
          this.userInputElement.click();
        }
      }]
    });
    await actionSheet.present();
  }

  captureImage() {
    this.nativeLib.captureImage(CameraSource.Camera);
    this.eventCustom.subscribe('imageReady', (imgData) => {
      if (imgData) {
        console.log('imgData', imgData);
        this.mediaArray.push(imgData);
        // this.utilServ.hideLoaderWait();
        // this.utilServ.hideLoaderWait();
        this.eventCustom.destroy('imageReady');
      }
    });
  }
  captureVideo() {
    this.nativeLib.captureVideo();
    this.eventCustom.subscribe('videoReady', (vidData) => {
      if (vidData) {
        console.log('vidData', vidData);
        this.mediaArray.push(vidData);
        // this.utilServ.hideLoaderWait();
        // this.utilServ.hideLoaderWait();
        this.eventCustom.destroy('videoReady');
      }
    });
  }

  pickImageVideoFromDevice(event) {
    console.log("myFile", event);
    const x = { mediaUrl: null, blob: null, filename: null, type: null };
    if (event.target.files && event.target.files[0]) {
      const myFile = event.target.files;

      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < myFile.length; i++) {
        x.blob = new Blob([myFile[i]], { type: myFile[i].type });
        x.mediaUrl = this.showImage(x.blob);
        x.filename = this.formateFileName(myFile[i].name);
        x.type = myFile[i].type;
        console.log(this.mediaArray)
        const innit = setInterval(() => {
          if (x.mediaUrl !== null) {
            this.mediaArray.push(x);
            clearInterval(innit);
          }
        }, 2);
      }

    }
  }
  showImage(image) {
    const imageURL = window.URL.createObjectURL(image);
    return this.sanitizer.bypassSecurityTrustUrl(imageURL);
  }
  formateFileName(filename) {
    let nameBeforeSpace: string = (filename.split(' '))[0];
    nameBeforeSpace = nameBeforeSpace.replace(/\s/g, '_');
    nameBeforeSpace = nameBeforeSpace.replace(/\./g, '_');
    const ext = (filename.split('.')).pop();
    const newFileName = this.userDetail.id + '_' + (this.utilServ.getTimeStamp()) + '_' + nameBeforeSpace + '.' + ext;
    return newFileName;
  }
  removeimg(imgId, i) {
    if (imgId) {
      const temp = {
        id: imgId,
      };
      this.ifEditied = true;
      this.mediaArray.push(temp);
    }
    this.mediaArray.splice(i, 1);
    console.log(this.mediaArray);
  }
  async openMultiMedia(fullAsserts, i) {
    //   const xDx = {
    //     asserts: fullAsserts,
    //     currentIndex: i
    //   };
    //   localStorage.setItem('mediaArray', JSON.stringify(xDx));
    //   const modal = await this.modalcontrolller.create({
    //     component: MultimediaViewComponent,
    //     // cssClass: 'sharePOP',
    //     componentProps: {
    //       // custom_id: this.shareValue
    //       swipeToClose: true
    //     }
    //   });
    //   return await modal.present();
  }
  back() {
    this.modalController.dismiss();
  }
}
