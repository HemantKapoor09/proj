import { Component } from '@angular/core';

import { Platform, ModalController, MenuController, LoadingController, AlertController, NavController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


// Custom
import { environment } from '../environments/environment';
import { Socket } from 'ngx-socket-io';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Network } from '@ionic-native/network/ngx';
// import { Device } from '@ionic-native/device/ngx';
// import { SharePopComponent } from './components/share-pop/share-pop.component';
import { ApiService } from './services/api.service';
import { GenralUtilsService } from './services/genral-utils.service';
import { EventsCustomService } from './services/events-custom.service';
import { SearchInAppComponent } from './components/search-in-app/search-in-app.component';
import { SharePopComponent } from './components/share-pop/share-pop.component';
// import { SplashComponent } from './components/splash/splash.component';
// import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})

export class AppComponent {
  route: any;
  // Basic User Details
  userDetail = null;
  profileImageURL = null;
  profileFirstname = null;
  profileLastName = null;
  profilePicUrl = '../assets/defaultProfile.png';
  profileId = null;

  environment: any;

  // Strings
  shareString = 'Share you presence on Tribation';
  timelineString = 'timeline';
  searchString = 'search';
  teamString = 'teams';
  myprofileString = 'my profile';
  eventsString = 'events';
  helpcenterString = 'help center';
  logoutString = 'log out';
  doYouWantLogoutString = 'are you sure you want to logout?';
  areYouSureString = 'logout';
  cancelString = 'cancel';
  yesString = 'yes';
  scoutsToolString = 'Scout Tool';
  guardianString = 'Guardian';
  currentLocation;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private modalcontrolller: ModalController,
    private menu: MenuController,
    private socket: Socket,
    private loadingController: LoadingController,
    // private device: Device,
    private alertController: AlertController,
    // private screenOrientation: ScreenOrientation,
    private navCtrl: NavController,
    private router: Router,
    private actRouter: ActivatedRoute,
    private network: Network,
    private location: Location,
    private eventCustom: EventsCustomService,
    private toastController: ToastController,
    private apiService: ApiService,
    private utilServ: GenralUtilsService) {
    this.actRouter.queryParams.subscribe(() => {
      localStorage.setItem('firstTime', 'true');
      this.userDetail = JSON.parse(localStorage.getItem('userdetail'));
      // alert('userDetail' + this.userDetail);
      setInterval(() => {
        if (this.userDetail) {
          this.socket.emit('login', { id: this.userDetail.tokenid });
        }
      }, 2200);
      setTimeout(() => {
        const init = setInterval(() => {
          if (this.profileFirstname && this.profileLastName) {
            clearInterval(init);
          } else {
            this.setupBasicDetails();
          }
        }, 400);
      }, 3000);
    });

    this.environment = environment;
    this.router.events.subscribe((val) => {
      this.currentLocation = this.location.path();
      localStorage.setItem('currentLocation', JSON.stringify(this.currentLocation));
      this.route = this.currentLocation;
    });
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      console.log('ready' + JSON.stringify(this.currentLocation));
      // this.setupBasicDetails();
      this.statusBar.styleDefault();
      if (this.userDetail === null) {
        this.userDetail = JSON.parse(localStorage.getItem('userdetail'));
        this.setupBasicDetails();
      }
      if (this.platform.is('android')) {
        this.statusBar.overlaysWebView(false);
        this.statusBar.backgroundColorByHexString('#000000');
      }

    });
    this.platform.pause.subscribe(() => {// background
      // this.setupBasicDetails();
      this.socket.emit('socketDisconnect', { id: this.userDetail.tokenid }, (e => {
        console.log('This is soket disconnect', e);
      }));
      if (this.userDetail === null) {
        this.userDetail = JSON.parse(localStorage.getItem('userdetail'));
        this.setupBasicDetails();
      }
    });

    this.platform.resume.subscribe(() => {// foreground
      // this.setupBasicDetails();
      if (this.userDetail === null) {
        this.userDetail = JSON.parse(localStorage.getItem('userdetail'));
        this.setupBasicDetails();
      }
    });
    this.socket.on('reloadApp', () => {
      this.utilServ.setUpUser();
      setTimeout(() => {
        this.splashScreen.show();
        this.utilServ.checkBasicElseRelodApp();
      }, 1000);
    });
    this.splashScreen.hide();
  }

  setupBasicDetails() {
    localStorage.setItem('online', 'true');
    if (this.userDetail) {
      this.socket.emit('login', { id: this.userDetail.tokenid });
      const data = {
        userId: this.userDetail.id,
        email: this.userDetail.email
      };
      this.socket.emit('findUser', data);
    }
    this.apiService.connectFuntion();
    this.utilServ.setUpUser();
    this.userDetail = JSON.parse(localStorage.getItem('userdetail'));
    this.eventCustom.subscribe('userDetail', (e) => {

      if (this.userDetail !== null) {
        this.apiService.connectFuntion();
        this.profileId = this.userDetail.id;
        this.profileImageURL = this.userDetail.profile_img_url;
        this.profileFirstname = this.userDetail.first_name;
        this.profileLastName = this.userDetail.last_name;

        this.utilServ.languageSetup();
        this.apiService.connectFuntion();
        if (this.utilServ.langLibrary) {
          this.searchString = this.utilServ.getLangByCode('search');
          this.teamString = this.utilServ.getLangByCode('team');
          this.myprofileString = this.utilServ.getLangByCode('myprofile');
          this.eventsString = this.utilServ.getLangByCode('events');
          this.helpcenterString = this.utilServ.getLangByCode('help');
          this.logoutString = this.utilServ.getLangByCode('logout');
          this.doYouWantLogoutString = this.utilServ.getLangByCode('do_you_want_logout');
          this.areYouSureString = this.utilServ.getLangByCode('are_you_sure');
          this.cancelString = this.utilServ.getLangByCode('cancel');
          this.yesString = this.utilServ.getLangByCode('yes');
          this.timelineString = this.utilServ.getLangByCode('timeline');
        }
      }
    });
    if (JSON.parse(localStorage.getItem('firstTime'))) {
      this.utilServ.userAddonDetails();
      this.utilServ.setSportsList();
      this.utilServ.getCountryList();
      localStorage.removeItem('firstTime');
    }
  }

  async sahreInvite() {
    const modal = await this.modalcontrolller.create({
      component: SharePopComponent,
      cssClass: 'sharePOP',
      componentProps: {
        // custom_id: this.shareValue
        swipeToClose: true
      }
    });
    return await modal.present();
  }

  async logout() {
    this.eventCustom.destroy('userDetail');
    const alert = await this.alertController.create({
      header: this.areYouSureString,
      message: this.doYouWantLogoutString,
      buttons: [
        {
          text: this.cancelString,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.menu.close();
          }
        }, {
          text: this.yesString,
          handler: () => {
            this.menu.close();
            this.userDetail = JSON.parse(localStorage.getItem('userdetail'));
            this.socket.emit('socketDisconnect', { id: this.userDetail.tokenid });
            const tokenlog = JSON.parse(localStorage.getItem('userdetail'));
            this.apiService.logout(tokenlog.token).pipe().subscribe((res: any) => {
              alert.dismiss();
              this.utilServ.userDetail = null;
              this.utilServ.friendsList = null;
              this.utilServ.teamData = null;
              localStorage.clear();
              localStorage.setItem('intro_status', JSON.stringify('1'));
              this.utilServ.setUpUser();
              this.utilServ.navLogin();
            });
          }
        }
      ]
    });
    await alert.present();
  }

  async searchFun() {
    const modal = await this.modalcontrolller.create({
      component: SearchInAppComponent,
      componentProps: {
        // custom_id: this.shareValue
        swipeToClose: true
      }
    });
    return await modal.present();
  }
  myProfile() {
    this.router.navigate([`/profile/${this.profileId}`]);
  }
  async dummSplash() {
    // const modal = await this.modalcontrolller.create({
    //   component: SplashComponent,
    //   componentProps: {
    //     swipeToClose: true
    //   }
    // });
    // return await modal.present();
  }
}
