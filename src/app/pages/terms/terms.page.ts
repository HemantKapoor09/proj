import { Component, OnInit } from '@angular/core';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { ApiService } from 'src/app/services/api.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.page.html',
  styleUrls: ['./terms.page.scss'],
})
export class TermsPage implements OnInit {
  language: any = 'English';
  getlanguage: any;
  termsContent: any;
  termsConditions: any;

  constructor(private apiService: ApiService,
    private utilServ: GenralUtilsService,) {

    this.language = localStorage.getItem("language");
    console.log(this.language)
    if (this.language == null || this.language == '') {
      this.language = 'English';
    }

    this.apiService.languageFile(this.language).pipe(first()).subscribe((res: any) => {
      this.getlanguage = res.message.Sheet1;
      for (var i = 0; i < this.getlanguage.length; i++) {
        if (this.getlanguage[i].code == 'terms_content') {
          this.termsContent = this.getlanguage[i].response
        //  console.log(this.termsContent);
        }
        else if (this.getlanguage[i].code == 'termsaccept_1') {
          this.termsConditions = this.getlanguage[i].response
        }

      }
    })

  }
  goback() {
    this.utilServ.navSignup();
  }

  ngOnInit() {
  }

}
