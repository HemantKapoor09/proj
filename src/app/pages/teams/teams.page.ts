import { Component, OnInit } from '@angular/core';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { ApiService } from 'src/app/services/api.service';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.page.html',
  styleUrls: ['./teams.page.scss'],
})
export class TeamsPage implements OnInit {
  // String
  teamString = 'Team';
  allTeamString = 'All Teams';
  manageString = 'Manage';
  searchString = 'Search';
  cancelString = 'Cancel';
  deleteString = 'Delete';
  okString = 'Ok';
  deleteTeamsString = 'Are you sure you want to delete this Team?';
  messageDeletedString = 'Team Deleted!';

  environment: any;

  userDetail;
  checkedsegment = 'allTeam';
  noTeamString = 'No Teams';
  allTeamList;
  myTeamList;
  noTeams = false;
  noOwnTeam = false;
  animationLoder = true;
  constructor(
    private router: Router,
    public socket: Socket,
    private alertController: AlertController,
    private apiService: ApiService,
    private actRouter: ActivatedRoute,
    private utilServ: GenralUtilsService,
  ) {
    this.actRouter.queryParams.subscribe(() => {
      this.utilServ.checkUserExists();
      this.userDetail = JSON.parse(this.utilServ.getUserDetails());
      this.allTeamListFun();
      this.socket.on('invite-team-member', (data) => {
        this.allTeamListFun();
      });
    });
  }

  ngOnInit() {
    this.environment = environment;
  }

  ionViewWillEnter() {
  }

  segmentChanged(ev) {
    this.checkedsegment = ev.detail.value;
  }
  allTeamListFun() {
    this.apiService.getTeamList(this.userDetail.id).pipe().subscribe((allTeam: any) => {
      this.apiService.connectFuntion();
      this.allTeamList = allTeam.message;
      if (this.allTeamList === null || this.allTeamList === '' || this.allTeamList.length === undefined) {
        this.noTeams = true;
        this.animationLoder = false;
      } else {
        this.animationLoder = false;
      }
    }, error => {
      this.utilServ.networkError();
    });
    setTimeout(() => {
      this.myTeam();
    }, 500);
  }

  myTeam() {
    this.apiService.getMyTeamList(this.userDetail.id).subscribe((myTeam: any) => {
      this.apiService.connectFuntion();
      this.myTeamList = myTeam.message;
      if (myTeam.message === 0) {
        this.noOwnTeam = true;
        this.animationLoder = false;
      } else {
        this.animationLoder = false;
        this.noOwnTeam = false;
      }
    }, error => {
      this.utilServ.networkError();
    });
  }
  aboutTeam(teamToShow) {
    localStorage.setItem('teamData', JSON.stringify(teamToShow));
    this.router.navigate(['/teams/about-team']);
  }
  async deleteTeams(teamId) {
    const confirm = await this.alertController.create({
      header: this.deleteString + '?',
      cssClass: 'buttonCss',
      message: this.deleteTeamsString,
      buttons: [{
        text: this.cancelString,
        role: 'cancel',
      },
      {
        text: this.okString,
        handler: () => {
          this.apiService.deleteTeam(teamId, this.userDetail.id).subscribe((res: any) => {
            console.log('deleteChats', res);
            if (res.success === 1) {
              this.allTeamListFun();
              this.utilServ.presentToast(this.messageDeletedString);
            }
          }, error => {
            this.utilServ.networkError();
          });
        }
      }
      ]
    });
    confirm.present();
  }
}
