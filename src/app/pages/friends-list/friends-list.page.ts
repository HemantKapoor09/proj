import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { environment } from 'src/environments/environment';
import { AlertController } from '@ionic/angular';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-friends-list',
  templateUrl: './friends-list.page.html',
  styleUrls: ['./friends-list.page.scss'],
})
export class FriendsListPage implements OnInit {
  // Strings:
  searchString = 'Search';
  friendsString = 'Friends';
  unfriendString = 'Unfriend';
  unfriendConfirmString = 'Are you sure you want to unfriend ';
  cancelString = 'Cancel';
  okayString = 'OK';

  myfrndsList = null;
  userDetail;
  environment;

  constructor(
    private actRouter: ActivatedRoute,
    public socket: Socket,
    private router: Router,
    public alertController: AlertController,
    private apiService: ApiService,
    private utilServ: GenralUtilsService,) {
    this.actRouter.queryParams.subscribe(() => {
      this.utilServ.checkUserExists();
      if (!this.utilServ.friendsList) {
        this.utilServ.userAddonDetails();
      }
      this.userDetail = JSON.parse(this.utilServ.getUserDetails());
      this.myfrndsList = this.utilServ.friendsList;
      const inter = setInterval(() => {
        if (!this.myfrndsList) {
          this.myfrndsList = this.utilServ.friendsList;
        } else {
          clearInterval(inter);
        }
      }, 2);
    });
  }
  ngOnInit() {
    this.environment = environment;
  }
  async unFriend(frndId, frndName) {
    const confirm = await this.alertController.create({
      header: this.unfriendString + '?',
      // message: this.unfriendConfirmString.replace('%X', frndName),
      message: this.unfriendConfirmString + frndName + '?',
      buttons: [
        {
          text: this.cancelString,
          role: 'cancel'
        },
        {
          text: this.okayString,
          handler: () => {
            const data = {
              cf_id: frndId,
              hf_id: this.userDetail.id
            };
            this.socket.connect();
            this.socket.emit('cancel-friend-request', data);
            // this.myfrndsList = '';
            this.utilServ.friendsList = null;
            this.utilServ.userAddonDetails();
            this.myfrndsList = this.utilServ.friendsList;
          }
        }
      ]
    });
    confirm.present();
  }
  rootMsgPage(friendId) {
    this.utilServ.navChatwithId(friendId);
  }
  goToProfile(id) {
    this.router.navigate([`profile/${id}`]);
  }
}
