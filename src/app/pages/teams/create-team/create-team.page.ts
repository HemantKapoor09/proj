import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { environment } from 'src/environments/environment';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { DeviceNativeApiService } from 'src/app/services/device-native-api.service';
import { AddTeamMemberComponent } from 'src/app/components/add-team-member/add-team-member.component';
import { ModalController } from '@ionic/angular';
import { EventsCustomService } from 'src/app/services/events-custom.service';
import { ApiService } from '../../../services/api.service';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.page.html',
  styleUrls: ['./create-team.page.scss'],
})
export class CreateTeamPage implements OnInit {
  createTeam: FormGroup;

  environment;
  imageprofile;
  teamData;
  teamPic = '';
  userDetail;

  // Strings
  typeTeamDescString = 'Type your team description (Optional)';
  typeTeamNameString = 'Type your Team Name';
  nextString = 'Next';
  mustEnterTeamnameString = 'Must Enter Team Name';
  noSpaceString = 'No space';

  constructor(
    private nativeLib: DeviceNativeApiService,
    private eventCustom: EventsCustomService,
    private modalController: ModalController,
    private apiService: ApiService,
    private location: Location,
    private utilServ: GenralUtilsService) {
    this.environment = environment;
    this.imageprofile = `${environment.apiUrl}v1/api/post/imagecall_mobile_url?imagepath=profile/cover1.jpg`;

  }

  ngOnInit() {
    this.createform();
    this.userDetail = JSON.parse(this.utilServ.getUserDetails());
  }

  private createform() {
    this.createTeam = new FormGroup({
      teamName: new FormControl('', [Validators.required, Validators.pattern('.*\\S.*[a-zA-z0-9 ]{2,}')]),
      teamDescription: new FormControl('', []),
    });
  }

  ionViewWillLeave() {
    this.createTeam.reset();
  }

  async addMembers() {
    this.teamData = {
      teamName: this.createTeam.value.teamName,
      teamDesc: this.createTeam.value.teamDescription,
      teamPicUrl: this.teamPic
    };
    localStorage.setItem('teamData', JSON.stringify(this.teamData));
    const modal = await this.modalController.create({
      component: AddTeamMemberComponent,
      componentProps: {
        custom_data: null
      }
    });
    modal.onDidDismiss().then((code) => {
      this.back();
    });
    return await modal.present();
  }
  presentActionSheet() {
    this.nativeLib.presentActionSheetCamOrGall();
    this.eventCustom.subscribe('imageReady', (imgData) => {
      this.imageprofile = imgData.webPath;
      this.apiService.uploadTeamGroupCover(imgData.blob, this.userDetail.id).pipe().subscribe((data: any) => {
        if (data.success === 1) {
          this.teamPic = String(data.message);
        } else { console.log('can not get image data'); }
      },
        (error: any) => {
          console.log('Error' + '' + 'server Issue');
        });
    });
  }

  back() {
    this.location.back();
  }
}
