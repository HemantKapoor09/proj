import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
import { environment } from '../../../environments/environment';
import { LoadingController, ModalController, AlertController } from '@ionic/angular';
import { Socket } from 'ngx-socket-io';
import { ActivatedRoute } from '@angular/router';
import { AddGroupChatMembersComponent } from 'src/app/components/add-group-chat-members/add-group-chat-members.component';
import { EventsCustomService } from 'src/app/services/events-custom.service';
import { Console } from 'console';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  chatArray = [];
  groupChatArray: any[];
  data = [];
  filterGroup = [];
  noLableGroup = [];
  onlineusers: [];
  searchTermGrp: any;
  searchTerm: any;
  checkedsegment = 'directChat';
  peoplesearch = 'people-search.png';
  chatrequesthidden = true;
  chatmessages: any;
  environment: any;
  usersdetail: any;
  profile_img_url: any = '';
  groupArray: any;

  // String
  chatString = 'Chats';
  messageString = 'Message';
  groupString = 'Groups';
  noChatString = 'No Chat';
  NoNameString = 'No name';
  mediaString = 'Media';
  searchString = 'Search';
  deleteString = 'Delete';
  cancelString = 'Cancel';
  okString = 'Ok';
  deleteDirectChatsString = 'Are you sure you want to delete this chats?';
  deleteGroupChatsString = 'Are you sure you want to delete this group chats?';
  messageDeletedString = 'Message Deleted!';


  newarray: any;
  chatlist: { content: any; created: any;
    first_name: any; from_id: any; profile_img_url: any; type: any; unreaded: number; user_type: string; };
  receivedChatUId = [];

  constructor(
    private socket: Socket,
    private modalController: ModalController,
    private loadingController: LoadingController,
    private apiService: ApiService,
    private alertController: AlertController,
    private eventCustom: EventsCustomService,
    private actRouter: ActivatedRoute,
    private utilServ: GenralUtilsService) {

    this.actRouter.params.subscribe(data => {
      this.apiService.connectFuntion();
      this.searchTermGrp = '';
      this.searchTerm = '';
      this.usersdetail = JSON.parse(this.utilServ.getUserDetails());
      this.refreshDirectChat();
      this.refreshGroupchat();
    });
  }

  ngOnInit() {
    this.environment = environment;

    this.socket.on('onlineusers', (data) => {
      data.forEach(ele => {
        this.chatArray.forEach( e => {
          if (e.from_id === ele.userid && ele.online === true){
            e.online = true;
          }
        });
      });
      this.onlineusers = data;
      // console.log(this.onlineusers);
    });
    this.actRouter.params.subscribe(() => {
      this.apiService.connectFuntion();
      this.socket.on('receive-message', (msg) => {
        this.apiService.connectFuntion();
        this.refreshDirectChat();
      });

      this.socket.on('receive-group-message', (data) => {
        this.apiService.connectFuntion();
        this.refreshGroupchat();
      });
    });
  }

  ionViewWillEnter() {
    this.utilServ.checkUserExists();
    this.apiService.connectFuntion();
  }

  refreshGroupchat() {
    this.apiService.getGroupChatList(this.usersdetail.id).pipe().subscribe((res: any) => {
      this.apiService.connectFuntion();
      this.groupArray = res.message;
      this.groupChatArray = this.groupArray;
      console.log(this.groupChatArray);
    });
  }

  refreshDirectChat() {
    this.apiService.getRecentMessages(this.usersdetail.id).pipe().subscribe(async (res: any) => {
      console.log('resinchars', res.message);
      if (this.chatmessages === 'No Friends') {
      }else{
        this.chatmessages = await res.message;
        this.chatArray = this.chatmessages;
      }
      // this.chatmessages = this.uniqBy(this.chatmessages, JSON.stringify);
      // const chatarray = [];
      // // tslint:disable-next-line: prefer-for-of
      // for (let v = 0; v < this.chatmessages.length; v++) {
      //   if (this.chatmessages[v].content !== '' && this.chatmessages[v].from_id !== this.usersdetail.id) {
      //     this.chatmessages[v].first_name = this.chatmessages[v].first_name + ' ' + this.chatmessages[v].last_name;
      //     chatarray.push(this.chatmessages[v]);
      //   } else if (this.chatmessages[v].type === 'media' && this.chatmessages[v].from_id !== this.usersdetail.id) {
      //     this.chatmessages[v].first_name = this.chatmessages[v].first_name + ' ' + this.chatmessages[v].last_name;
      //     chatarray.push(this.chatmessages[v]);
      //   }
      // }
      // this.chatArray = this.removeDuplicates(chatarray, 'from_id');
      // this.chatArray.sort((a, b): any => {
      //   return new Date(b.created).getTime() - new Date(a.created).getTime();
      // });
      // // tslint:disable-next-line: prefer-for-of
      // for (let i = 0; i < this.chatArray.length; i++) {
      //   if (this.chatArray.hasOwnProperty('online')) { break; } else {
      //     // tslint:disable-next-line: prefer-for-of
      //     for (let j = 0; j < this.onlineusers.length; j++) {
      //       if (this.onlineusers[j].userid === this.chatArray[i].from_id && this.onlineusers[j].online === true) {
      //         this.chatArray[i].online = 1;
      //       }
      //     }
      //   }
      // }
      // // tslint:disable-next-line: prefer-for-of
      // for (let i = 0; i < this.chatmessages.length; i++) {
      //   if (this.chatmessages[i].unreaded !== 0) {
      //     this.data.push(this.chatmessages[i]);
      //     this.data = this.uniqBy(this.data, JSON.stringify);
      //     this.data = this.removeDuplicates(this.data, 'from_id');
      //   }
      // }
    });
  }

  // REMOVE DUPLICATES
  removeDuplicates(originalArray, prop) {
    const newArray = [];
    const lookupObject = {};
    for (const i in originalArray) {
      if (i) {
        lookupObject[originalArray[i][prop]] = originalArray[i];
      }
    }
    for (const i in lookupObject) {
      if (i) {
        newArray.push(lookupObject[i]);
      }
    }
    return newArray;
  }

  // UNIQ
  uniqBy(a, key) {
    const seen = {};
    return a.filter((item: any) => {
      const k = key(item);
      return seen.hasOwnProperty(k) ? false : (seen[k] = true);
    });
  }

  // OPEN DIRECT CHAT
  chatdetailClick(id) {
    this.utilServ.navChatwithId(id);
  }

  // OPEN GROUP CHAT
  groupchatdetail(data) {
    const x = data;
    x.teamid = data.id;
    localStorage.setItem('teamData', JSON.stringify(x));
    if (data.type === 1) {
      this.utilServ.navGroupChatwithId(data.id);
    } else if (data.type === 2) {
      this.utilServ.navGroupChatwithId(data.gid);
    }
  }

  setFilteredItems(searchTerm) {
    // this.apiService.getRecentMessages(this.usersdetail.id).pipe().subscribe((res: any) => {
    //   this.items = res.message;
    //   this.items = searchTerm;
    // });
  }

  async createGroup() {
    const modal = await this.modalController.create({
      component: AddGroupChatMembersComponent
    });
    modal.onDidDismiss().then((ref) => {
      this.refreshGroupchat();
      // if (ref.data.data === 0) {
      // }
    });
    return await modal.present();
  }

  // Search on direct chat
  searchDirChat(x) {
    this.chatArray = this.filterDireChat(this.chatmessages, x || null);
  }

  // search on group
  searchGrp(x) {
    this.groupChatArray = this.filterGrp(this.groupArray, x || null);
  }

  filterDireChat(items: any[], terms: string): any[] {
    if (!items) { return []; }
    if (!terms) { return items; }
    terms = terms.toLowerCase();
    return items.filter(it => {
      return it.first_name.toLowerCase().includes(terms);
    });
  }

  filterGrp(items: any[], terms: string): any[] {
    if (!items) { return []; }
    if (!terms) { return items; }
    terms = terms.toLowerCase();
    return items.filter(it => {
      return it.team_name.toLowerCase().includes(terms);
    });
  }

  segmentChanged(ev) {
    this.checkedsegment = ev.detail.value;
  }
  async deleteGroupChats(groupId) {
    const confirm = await this.alertController.create({
      header: this.deleteString + '?',
      cssClass: 'buttonCss',
      message: this.deleteGroupChatsString,
      buttons: [{
        text: this.cancelString,
        role: 'cancel',
      },
      {
        text: this.okString,
        handler: () => {
          this.apiService.deleteGroupChat(this.usersdetail.id, groupId).pipe().subscribe((res: any) => {
            console.log('deleteGroupChats', res);
            if (res.success === 1) {
              this.utilServ.presentToast(this.messageDeletedString);
            }
          });
        }
      }
      ]
    });
    confirm.present();
  }
  async deleteChats(fromId) {
    const confirm = await this.alertController.create({
      header: this.deleteString + '?',
      cssClass: 'buttonCss',
      message: this.deleteDirectChatsString,
      buttons: [{
        text: this.cancelString,
        role: 'cancel',
      },
      {
        text: this.okString,
        handler: () => {
          this.apiService.deleteChat(this.usersdetail.id, fromId).pipe().subscribe((res: any) => {
            console.log('deleteChats', res);
            if (res.success === 1) {
              this.utilServ.presentToast(this.messageDeletedString);
            }
          });
        }
      }
      ]
    });
    confirm.present();

  }
}
