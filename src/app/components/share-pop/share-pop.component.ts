import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-Sharing/ngx';
// import { File } from '@ionic-native/File/ngx';
import { Plugins, FilesystemDirectory, FilesystemEncoding } from '@capacitor/core';
const { Filesystem } = Plugins;

import { ActivatedRoute } from '@angular/router';
import { GenralUtilsService } from 'src/app/services/genral-utils.service';
@Component({
  selector: 'app-share-pop',
  templateUrl: './share-pop.component.html',
  styleUrls: ['./share-pop.component.scss'],
})
export class SharePopComponent implements OnInit {
  url = 'https://bit.ly/2WXMO2d';
  usersDetail: any = '';
  username: string = '';
  head: string = '';
  gb: string = '';
  teweetMessage: string = '';
  errorMessage = ' app not installed on your Device! ';
  text = '\n Tribation is a social media platform geared towards empowering athletes and building strong communities around the sports they love. \n Tribation gives aspiring athletes a new avenue to achieve their dreams with its Athlete Scouting Platform, which allows registered and approved talent scouts to contact promising athletes directly and help them rise to higher levels of sportsmanship.\n\n Downlod your app now \n\t Andriod: play.google.com/store/apps/details?id=com.tribation.sportapp \n\t iOS: apps.apple.com/ca/app/tribation/id1460175372 \n\t or Visit us at: www.tribation.com \n';
  constructor(
    public modalController: ModalController,
    private utilServ: GenralUtilsService,
    // private file: File,
    private socialSharing: SocialSharing,
    private route: ActivatedRoute) {
    this.route.queryParams.subscribe(x => {
      this.usersDetail = JSON.parse(this.utilServ.getUserDetails());
      console.log(this.usersDetail);
      this.username = (this.usersDetail.first_name + ' ' + this.usersDetail.last_name);
      this.head = ('Join ' + this.username + ' at Tribation a free app for athletes getting noticed!\n');
      this.gb = ('\n For Global Access Link: ' + this.url);
      this.teweetMessage = (this.head + '\n Downlod your app now \n\t Andriod: play.google.com/store/apps/details?id=com.tribation.sportapp \n\t iOS: apps.apple.com/ca/app/tribation/id1460175372 \n\t or Visit us at: www.tribation.com \n' + this.gb);
    });
  }

  ngOnInit() { }

  ionViewWillLeave() {
    this.close();
  }

  close() {
    this.modalController.dismiss();
  }

  async tweet() {
    this.socialSharing.shareViaTwitter(this.teweetMessage, null, null).then(() => {
    }).catch(e => {
      alert('Twitter' + this.errorMessage);
    });
  }

  async email() {
    let fileName: any = await this.attach();
    // fileName.getUriResult();
    this.socialSharing.shareViaEmail((this.head + this.text), this.head, null, null, null, fileName).then(() => {
      // this.file.removeFile(this.file.cacheDirectory, fileName.name);
    }).catch(e => {
      alert('Email' + this.errorMessage);
    });
  }

  async fbShare() {
    this.socialSharing.shareViaFacebookWithPasteMessageHint(this.url, null, null, 'Add message for you Friends').then(() => {
    }).catch(e => {
      alert('Facebook' + this.errorMessage);
    });
  }

  async whatsapp() {
    this.socialSharing.shareViaWhatsApp(this.head.concat(this.text + this.gb), null, null).then(() => {
    }).catch(e => {
      alert(e);
      alert('Whatsapp' + this.errorMessage);
    });
  }

  async sms() {
    this.socialSharing.shareViaSMS(this.head.concat(this.text + this.gb), null).then(() => {
    }).catch(e => {
      alert('Messaging' + this.errorMessage);
    });
  }

  async insta() {
    this.socialSharing.shareViaInstagram(this.head.concat(this.text + this.gb), null).then(() => {
    }).catch(e => {
      alert('Instagram' + this.errorMessage);
    });
  }

  async attach() {
    try {
      // This example copies a file within the documents directory
      let ret = await Filesystem.copy({
        from: '../../../assets/tribation.jpg',
        to: `${new Date().getTime()}.jpg`,
        directory: FilesystemDirectory.Documents
      });
      return ret;
    } catch (e) {
      alert('Unable to copy file' + JSON.stringify(e));
    }

    // return this.file.copyFile(`${this.file.applicationDirectory}www/assets`, 'tribation.jpg', this.file.cacheDirectory, `${new Date().getTime()}.jpg`);
  }
}
